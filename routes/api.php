<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Unprotected routes
Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function (\Illuminate\Routing\Router $router) {
    Route::post('login', '\App\Http\Controllers\AuthController@login');
    Route::post('logout', '\App\Http\Controllers\AuthController@logout');
});

Route::group([
    'middleware' => 'api',
], function (\Illuminate\Routing\Router $router) {
    Route::post('users', '\App\Http\Controllers\UserController@store');
});


// JWT Protected routes
Route::group(array(
    'middleware' => ['api', 'jwt.auth'],
), function () {
    Route::get('auth/me', '\App\Http\Controllers\UserController@show');
    Route::get('users', '\App\Http\Controllers\UserController@index');
    Route::put('users', '\App\Http\Controllers\UserController@update');
    Route::put('users/change-password', '\App\Http\Controllers\UserController@changePassword');
    Route::delete('users', '\App\Http\Controllers\UserController@destroy');
    Route::get('conversations', '\App\Http\Controllers\ConversationController@index');
    Route::post('conversations', '\App\Http\Controllers\ConversationController@store');
    Route::delete('conversations/{id}', '\App\Http\Controllers\ConversationController@destroy');
    Route::post('conversations/{id}/user', '\App\Http\Controllers\ConversationController@addUserToConversation');
    Route::get('conversations/{id}', '\App\Http\Controllers\ConversationController@show');
    Route::post('conversations/{id}/messages', '\App\Http\Controllers\ConversationController@addMessage');
});

