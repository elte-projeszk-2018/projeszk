<?php

namespace App\Http\Requests;

use App\User;

/**
 * @SWG\Definition(
 *         definition="ChangePasswordRequest",
 *         required={"old_password", "new_password"},
 *         @SWG\Property(
 *             property="old_password",
 *             description="The old password",
 *             type="string",
 *         ),
 *         @SWG\Property(
 *             property="new_password",
 *             description="the new password",
 *             type="string",
 *         ),
 * )
 */
class ChangePasswordRequest extends RestRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password' => User::$rules['password'],
            'new_password' => User::$rules['password'],
        ];
    }
}
