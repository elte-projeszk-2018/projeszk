<?php

namespace App\Http\Requests;

use App\User;

/**
 * @SWG\Definition(
 *         definition="RegisterUserRequest",
 *         required={"first_name", "last_name", "email", "password"},
 *         @SWG\Property(
 *             property="first_name",
 *             description="first_name",
 *             type="string",
 *         ),
 *         @SWG\Property(
 *             property="last_name",
 *             description="last_name",
 *             type="string",
 *         ),
 *         @SWG\Property(
 *             property="email",
 *             description="email",
 *             type="string",
 *             format="email"
 *         ),
 *         @SWG\Property(
 *             property="password",
 *             description="password",
 *             type="string",
 *         ),
 * )
 */
class RegisterUserRequest extends RestRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return User::$rules;
    }
}
