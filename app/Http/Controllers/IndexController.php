<?php

namespace App\Http\Controllers;

use File;

class IndexController extends Controller
{
    public function index()
    {
        return File::get(public_path() . '/index.php');
    }
}
