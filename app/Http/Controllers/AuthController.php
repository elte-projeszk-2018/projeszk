<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Services\AuthService;
use App\Services\UserService;
use Auth;

class AuthController extends RestController
{
    /** @var UserService */
    private $userService;

    /** @var AuthService */
    private $authService;

    public function __construct(UserService $userService, AuthService $authService)
    {
        $this->userService = $userService;
        $this->authService = $authService;
    }

    /**
     * @SWG\Post(
     *      path="/api/auth/login",
     *      summary="Login route",
     *      tags={"authorization"},
     *      description="Login route",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Login request body",
     *          required=true,
     *          @SWG\Schema(
     *              ref="#/definitions/LoginRequest",
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  @SWG\Items(
     *                      ref="#/definitions/LoginResponseData"
     *                  )
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *     @SWG\Response(
     *          response=401,
     *          description="unauthorized",
     *          @SWG\Schema(
     *              ref="#/definitions/ValidationFailedException"
     *          )
     *      ),
     *     @SWG\Response(
     *          response=412,
     *          description="validation_failed",
     *          @SWG\Schema(
     *              ref="#/definitions/ValidationFailedException"
     *          )
     *      ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             ref="#/definitions/RestException"
     *         )
     *     ),
     * )
     *
     * @param LoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\UnauthorizedException
     */
    public function login(LoginRequest $request)
    {
        $token = $this->authService->login($request);
        return $this->buildResponse([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => config('jwt.ttl') * 60,
            'user' => $this->userService->getUser(),
        ]);
    }

    /**
     * @SWG\Post(
     *      path="/api/auth/logout",
     *      summary="Logout route",
     *      tags={"authorization"},
     *      description="Logout route",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="object",
     *                  default=""
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *     @SWG\Response(
     *         response="500",
     *         description="unexpected error",
     *         @SWG\Schema(
     *             ref="#/definitions/RestException"
     *         )
     *     ),
     * )
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        if (Auth::guard('api')->check()) {
            $this->authService->logout();
        }
        return $this->buildResponse("Logout successful!");
    }
}
