<?php

namespace App\Http\Controllers;

use App\Events\MessageSavedEvent;
use App\Exceptions\ForbiddenException;
use App\Exceptions\NotFoundException;
use App\Exceptions\ValidationFailedException;
use App\Http\Requests\AddMessageRequest;
use App\Http\Requests\AddUserToConversationRequest;
use App\Http\Requests\CreateConversationRequest;
use App\Models\Conversation;
use App\Services\ConversationService;
use App\Services\UserService;
use Event;

class ConversationController extends RestController
{
    /** @var ConversationService */
    private $conversationService;
    /** @var UserService */
    private $userService;

    public function __construct(ConversationService $conversationService, UserService $userService)
    {
        $this->conversationService = $conversationService;
        $this->userService = $userService;
    }


    public function index()
    {
        $user = $this->userService->getUser();

        return $this->buildResponse($user->conversations()->with(['admin', 'users', 'messages'])->get());
    }

    public function store(CreateConversationRequest $request)
    {
        $user = $this->userService->getUser();
        $userIds = array_unique($request->get('user_ids'));
        if (count($userIds) === 1 && in_array($user->id, $userIds)) {
            throw new ValidationFailedException();
        }
        $conversation = $this->conversationService->findConversation($userIds) ?? $this->conversationService->createConversation($userIds);
        return $this->buildResponse($conversation);
    }

    public function addUserToConversation(int $id, AddUserToConversationRequest $request)
    {
        $conversation = $this->getConversation($id);
        $userToAdd = $this->userService->find($request->get('user_id'));
        $conversation = $this->conversationService->addUserToConversation($conversation, $userToAdd);
        return $this->buildResponse($conversation);
    }

    public function show(int $id)
    {
        $conversation = $this->getConversation($id);
        $conversation->load(['admin', 'users', 'messages']);
        return $this->buildResponse($conversation);
    }

    public function addMessage(int $conversationId, AddMessageRequest $request)
    {
        $conversation = $this->getConversation($conversationId);
        $message = $request->get('message');
        $message = $this->conversationService->addMessage($conversation, $message);
        Event::fire(new MessageSavedEvent($message, $conversation, $this->userService->getUser()));
        return $this->buildResponse($message);
    }

    public function destroy(int $id)
    {
        $user = $this->userService->getUser();
        $conversation = $this->getConversation($id);
        if ($conversation->admin_id !== $user->id) {
            throw new ForbiddenException();
        }
        $conversation->delete();
        return $this->buildResponse('');
    }

    private function getConversation(int $id): Conversation
    {
        $user = $this->userService->getUser();
        /** @var Conversation $conversation */
        $conversation = $user->conversations()->find($id);
        if (!$conversation) {
            throw new NotFoundException();
        }
        return $conversation;
    }
}
