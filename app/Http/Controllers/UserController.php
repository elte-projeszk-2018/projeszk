<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\RegisterUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Services\AuthService;
use App\Services\UserService;
use App\User;

class UserController extends RestController
{
    /** @var UserService */
    private $userService;

    /** @var AuthService */
    private $authService;

    public function __construct(UserService $userService, AuthService $authService)
    {
        $this->userService = $userService;
        $this->authService = $authService;
    }
    /**
     * @SWG\Get(
     *      path="/api/users",
     *      summary="List all user",
     *      tags={"user"},
     *      description="Complete listing of all user",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          in="header",
     *          name="AUTHORIZATION",
     *          description="Authorization header (Bearer [access_token])",
     *          type="string",
     *          required=true,
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="array",
     *                  @SWG\Items(
     *                      ref="#/definitions/UserResponseDto"
     *                  )
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *     @SWG\Response(
     *          response=401,
     *          description="unauthorized",
     *          @SWG\Schema(
     *              ref="#/definitions/ValidationFailedException"
     *          )
     *      ),
     *     @SWG\Response(
     *         response=500,
     *         description="unexpected error",
     *         @SWG\Schema(
     *             ref="#/definitions/RestException"
     *         )
     *     ),
     * )
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return $this->buildResponse($this->userService->findAll());
    }

    /**
     * @SWG\Post(
     *      path="/api/users",
     *      summary="RegisterUser route",
     *      tags={"user"},
     *      description="Route to sign up and create a user",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="RegisterUser request body",
     *          required=true,
     *          @SWG\Schema(
     *              ref="#/definitions/RegisterUserRequest",
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  @SWG\Items(
     *                      ref="#/definitions/UserResponseDto"
     *                  )
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *     @SWG\Response(
     *          response=412,
     *          description="validation_failed",
     *          @SWG\Schema(
     *              ref="#/definitions/ValidationFailedException"
     *          )
     *      ),
     *     @SWG\Response(
     *         response=500,
     *         description="unexpected error",
     *         @SWG\Schema(
     *             ref="#/definitions/RestException"
     *         )
     *     ),
     * )
     *
     * @param RegisterUserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(RegisterUserRequest $request)
    {
        $user = new User([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'email' => $request['email'],
            'username' => $request['username'],
            'password' => $request['password'],
        ]);

        $user = $this->userService->createUser($user);
        return $this->buildResponse($user);
    }

    /**
     * @SWG\Get(
     *      path="/api/auth/me",
     *      summary="Get current user's data",
     *      tags={"user"},
     *      description="Get current user's data",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          in="header",
     *          name="AUTHORIZATION",
     *          description="Authorization header (Bearer [access_token])",
     *          type="string",
     *          required=true,
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="array",
     *                  @SWG\Items(
     *                      ref="#/definitions/UserResponseDto"
     *                  )
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *     @SWG\Response(
     *          response=401,
     *          description="unauthorized",
     *          @SWG\Schema(
     *              ref="#/definitions/ValidationFailedException"
     *          )
     *      ),
     *     @SWG\Response(
     *         response=500,
     *         description="unexpected error",
     *         @SWG\Schema(
     *             ref="#/definitions/RestException"
     *         )
     *     ),
     * )
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show()
    {
        return $this->buildResponse($this->authService->getUser());
    }

    /**
     * @SWG\Put(
     *      path="/api/users",
     *      summary="UpdateUser route",
     *      tags={"user"},
     *      description="Route to update a user's name",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          in="header",
     *          name="AUTHORIZATION",
     *          description="Authorization header (Bearer [access_token])",
     *          type="string",
     *          required=true,
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="UpdateUser request body",
     *          required=true,
     *          @SWG\Schema(
     *              ref="#/definitions/UpdateUserRequest",
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  @SWG\Items(
     *                      ref="#/definitions/UserResponseDto"
     *                  )
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *     @SWG\Response(
     *          response=401,
     *          description="unauthorized",
     *          @SWG\Schema(
     *              ref="#/definitions/UnauthorizedException"
     *          )
     *      ),
     *     @SWG\Response(
     *          response=412,
     *          description="validation_failed",
     *          @SWG\Schema(
     *              ref="#/definitions/ValidationFailedException"
     *          )
     *      ),
     *     @SWG\Response(
     *         response=500,
     *         description="unexpected error",
     *         @SWG\Schema(
     *             ref="#/definitions/RestException"
     *         )
     *     ),
     * )
     *
     * @param UpdateUserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateUserRequest $request)
    {
        $user = $this->userService->getUser();
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->save();

        return $this->buildResponse($user);
    }

    /**
     * @SWG\Put(
     *      path="/api/users/change-password",
     *      summary="ChangePassword route",
     *      tags={"user"},
     *      description="Route to change a user's password",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          in="header",
     *          name="AUTHORIZATION",
     *          description="Authorization header (Bearer [access_token])",
     *          type="string",
     *          required=true,
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ChangePassword request body",
     *          required=true,
     *          @SWG\Schema(
     *              ref="#/definitions/ChangePasswordRequest",
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string",
     *                  default=""
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *     @SWG\Response(
     *          response=401,
     *          description="unauthorized",
     *          @SWG\Schema(
     *              ref="#/definitions/UnauthorizedException"
     *          )
     *      ),
     *     @SWG\Response(
     *          response=412,
     *          description="validation_failed",
     *          @SWG\Schema(
     *              ref="#/definitions/ValidationFailedException"
     *          )
     *      ),
     *     @SWG\Response(
     *         response=500,
     *         description="unexpected error",
     *         @SWG\Schema(
     *             ref="#/definitions/RestException"
     *         )
     *     ),
     * )
     *
     * @param ChangePasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\ValidationFailedException
     */
    public function changePassword(ChangePasswordRequest $request)
    {
        $oldPassword = $request->get('old_password');
        $newPassword = $request->get('new_password');
        $this->userService->changePassword($oldPassword, $newPassword);
        return $this->buildResponse('');
    }

    /**
     * @SWG\Delete(
     *      path="/api/users",
     *      summary="DeleteUser route",
     *      tags={"user"},
     *      description="Route to delete self from the app",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          in="header",
     *          name="AUTHORIZATION",
     *          description="Authorization header (Bearer [access_token])",
     *          type="string",
     *          required=true,
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string",
     *                  default=""
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *     @SWG\Response(
     *          response=401,
     *          description="unauthorized",
     *          @SWG\Schema(
     *              ref="#/definitions/UnauthorizedException"
     *          )
     *      ),
     *     @SWG\Response(
     *         response=500,
     *         description="unexpected error",
     *         @SWG\Schema(
     *             ref="#/definitions/RestException"
     *         )
     *     ),
     * )
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy()
    {
        $user = $this->userService->getUser();
        $user->delete();
        return $this->buildResponse('');
    }
}
