<?php

/**
 * @SWG\Definition(
 *         definition="LoginResponseData",
 *         @SWG\Property(
 *             property="access_token",
 *             type="string"
 *         ),
 *         @SWG\Property(
 *             property="token_type",
 *             type="string"
 *         ),
 *         @SWG\Property(
 *             property="expires_in",
 *             type="string"
 *         ),
 *         @SWG\Property(
 *             ref="#/definitions/UserResponseDto",
 *             property="user"
 *         )
 * )
 */