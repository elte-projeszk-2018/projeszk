<?php

namespace App\Providers;

use Illuminate\Broadcasting\BroadcastManager;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Broadcast;

class BroadcastServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Broadcast::routes();

        require base_path('routes/channels.php');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('Illuminate\Broadcasting\BroadcastManager', function ($app) {
            return new BroadcastManager($app);
        });

        $this->app->singleton('Illuminate\Contracts\Broadcasting\Broadcaster', function ($app) {
            return $app->make('Illuminate\Broadcasting\BroadcastManager')->connection();
        });

        $this->app->alias(
            'Illuminate\Broadcasting\BroadcastManager', 'Illuminate\Contracts\Broadcasting\Factory'
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            'Illuminate\Broadcasting\BroadcastManager',
            'Illuminate\Contracts\Broadcasting\Factory',
            'Illuminate\Contracts\Broadcasting\Broadcaster',
        ];
    }
}
