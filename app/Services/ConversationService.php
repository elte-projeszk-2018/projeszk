<?php

namespace App\Services;

use App\Exceptions\DuplicateException;
use App\Exceptions\ForbiddenException;
use App\Models\Conversation;
use App\Models\Message;
use App\Repositories\ConversationRepository;
use App\Repositories\MessageRepository;
use App\User;
use DB;

class ConversationService
{
    /** @var UserService */
    private $userService;
    /** @var ConversationRepository */
    private $conversationRepository;
    /** @var MessageRepository */
    private $messageRepository;

    public function __construct(
        UserService $userService,
        ConversationRepository $conversationRepository,
        MessageRepository $messageRepository
    ) {
        $this->userService = $userService;
        $this->conversationRepository = $conversationRepository;
        $this->messageRepository = $messageRepository;
    }


    public function createConversation(array $userIds): Conversation
    {
        $user = $this->userService->getUser();
        $participants = array_map(function ($id) {
            return $this->userService->find($id);
        }, $userIds);
        $participants[] = $user;

        $conversation = new Conversation();
        $this->save($user, $participants, $conversation);
        return $conversation;
    }

    private function save(User $admin, array $users, Conversation $conversation): void
    {
        DB::transaction(function () use ($admin, $users, $conversation) {
            $conversation->admin()->associate($admin);
            $this->conversationRepository->save($conversation);
            foreach ($users as $user) {
                $user->conversations()->save($conversation);
            }
        });
    }

    public function findConversation(array $userIds): ?Conversation
    {
        $user = $this->userService->getUser();
        $userIds[] = $user->id;
        $userIds = array_unique($userIds);
        return $user->conversations()->get()->filter(function ($conversation) use ($userIds) {
            $users = $conversation->users()->get();
            $participantCount = $users->count();
            $otherUsers = $users->filter(function ($user) use ($userIds) {
                return !in_array($user->id, $userIds);
            });
            return $participantCount === count($userIds) && $otherUsers->count() === 0;
        })->first();
    }

    public function addUserToConversation(Conversation $conversation, User $user): Conversation
    {
        if ($conversation->users()->find($user->id)) {
            throw new ForbiddenException();
        }
        $participantIds = $conversation->users()->get()->add($user)->pluck('id');
        $existingConversation = $this->findConversation($participantIds->toArray());
        if ($existingConversation) {
            throw new DuplicateException();
        }
        $conversation->users()->save($user);
        return $conversation;
    }

    public function addMessage(Conversation $conversation, string $message): Message
    {
        $user = $this->userService->getUser();
        $message = new Message([
            'message' => $message,
        ]);
        DB::transaction(function () use ($message, $user, $conversation) {
            $message->owner()->associate($user);
            $message->conversation()->associate($conversation);
            $this->messageRepository->save($message);
        });

        return $message;
    }
}