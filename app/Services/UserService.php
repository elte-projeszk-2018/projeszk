<?php

namespace App\Services;

use App\Exceptions\ValidationFailedException;
use App\Repositories\UserRepository;
use App\User;
use Auth;
use Illuminate\Support\Collection;

class UserService
{
    /** UserRepository */
    private $userRepository;

    /** @var User */
    private $user = null;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function findAll(): Collection
    {
        return $this->userRepository->findAll();
    }

    public function find(int $id): User
    {
        return $this->userRepository->find($id);
    }

    public function getUser(): ?User
    {
        if (!$this->user && Auth::check()) {
            $this->user = Auth::user();
        }
        return $this->user;
    }

    public function createUser(User $user): User
    {
        $this->userRepository->create($user);
        return $user;
    }

    public function changePassword(string $oldPassword, string $newPassword): void
    {
        $user = $this->getUser();
        $auth = Auth::attempt(['email' => $user['email'], 'password' => $oldPassword]);
        if ($auth) {
            $user->password = $newPassword;
            $user->save();
        } else {
            throw new ValidationFailedException();
        }
    }
}