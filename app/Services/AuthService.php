<?php

namespace App\Services;

use App\Exceptions\UnauthorizedException;
use App\Http\Requests\LoginRequest;
use App\User;
use Auth;
use JWTAuth;
use Log;

class AuthService
{
    public function login(LoginRequest $request): string
    {
        $credentials = $request->only('email', 'password');
        Log::debug("User attempting to log in with the following email address: " . $credentials['email']);
        $this->checkUserExistence($credentials);
        // verify the credentials and create a token for the user
        if (!$token = JWTAuth::attempt($credentials)) {
            throw new UnauthorizedException("Invalid credentials", $id = "invalid_credentials", 401);
        }
        Log::debug("User logged in with the following email address: " . $credentials['email']);
        return $token;
    }

    public function logout()
    {
        JWTAuth::invalidate(JWTAuth::getToken());
    }

    public function getUser()
    {
        return Auth::guard()->user();
    }

    private function checkUserExistence(array $credentials): void
    {
        $user = User::whereEmail($credentials['email'])->first();
        if (!$user) {
            throw new UnauthorizedException("Invalid credentials", $id = "invalid_credentials", 401);
        }
    }
}