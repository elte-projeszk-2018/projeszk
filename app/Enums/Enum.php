<?php

namespace App\Enums;

interface Enum
{
    public function getConstName(): string;
    public function getConstValue();
}