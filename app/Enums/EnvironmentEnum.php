<?php

namespace App\Enums;

final class EnvironmentEnum extends BaseEnum
{
    const DEVELOPMENT = 'development';
    const LOCAL = 'local';
    const PRODUCTION = 'production';
    const TESTING = 'testing';
}