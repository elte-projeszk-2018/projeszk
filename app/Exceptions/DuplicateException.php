<?php

namespace App\Exceptions;

class DuplicateException extends RestException
{
    public function __construct($message = "Duplicate")
    {
        parent::__construct(
            "Duplicate",
            "duplicate",
            409,
            $message
        );
    }
}
