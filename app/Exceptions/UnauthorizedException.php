<?php

namespace App\Exceptions;

/**  @SWG\Definition(
 *   definition="UnauthorizedException",
 *   allOf={
 *     @SWG\Schema(ref="#/definitions/RestException"),
 *     @SWG\Schema(
 *          @SWG\Property(
 *              property="title",
 *              description="title",
 *              type="string",
 *              default="The user is not authenticated"
 *          ),
 *          @SWG\Property(
 *              property="id",
 *              description="id",
 *              type="string",
 *              default="unauthorized"
 *          ),
 *          @SWG\Property(
 *              property="status",
 *              description="HTTP response status",
 *              type="integer",
 *              default=401
 *          ),
 *     )
 *   },
 * )
 *
 */
class UnauthorizedException extends RestException
{
    public function __construct($message = "The user is not authenticated")
    {
        parent::__construct(
            "Unauthorized operation",
            "unauthorized",
            401,
            $message
        );
    }
}
