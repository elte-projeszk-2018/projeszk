<?php

namespace App\Exceptions;

/**  @SWG\Definition(
 *   definition="ValidationFailedException",
 *   allOf={
 *     @SWG\Schema(ref="#/definitions/RestException"),
 *     @SWG\Schema(
 *          @SWG\Property(
 *              property="title",
 *              description="title",
 *              type="string",
 *              default="Request validation failed"
 *          ),
 *          @SWG\Property(
 *              property="id",
 *              description="id",
 *              type="string",
 *              default="validation_failed"
 *          ),
 *          @SWG\Property(
 *              property="status",
 *              description="HTTP response status",
 *              type="integer",
 *              default=412
 *          ),
 *     )
 *   },
 * )
 *
 */
class ValidationFailedException extends RestException
{
    public function __construct($message = '')
    {
        parent::__construct(
            "Request validation failed",
            "validation_failed",
            412,
            $message
        );
    }
}
