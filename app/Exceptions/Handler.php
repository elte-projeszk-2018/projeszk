<?php

namespace App\Exceptions;

use App;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $exception
     * @return void
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     * @return \Illuminate\Http\JsonResponse | \Symfony\Component\HttpFoundation\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof RestException) {
            return Response::json([
                "id" => $exception->getId(),
                "error" => $exception->getMessage(),
                "details" => $exception->getDetail(),
                "status" => $exception->getStatus()
            ],
                $exception->getStatus());
        }

        if($exception instanceof NotFoundHttpException)
        {
            return redirect()->route('home');
        }

        if ($exception instanceof HttpException) {
            return Response::json([
                "id" => get_class($exception),
                "error" => $exception->getMessage(),
                "details" => $exception->getTrace(),
                "status" => $exception->getStatusCode()

            ],
                $exception->getStatusCode());
        }

        return parent::render($request, $exception);
    }
}
