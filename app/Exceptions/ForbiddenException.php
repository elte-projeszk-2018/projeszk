<?php

namespace App\Exceptions;

class ForbiddenException extends RestException
{
    public function __construct($message = "Forbidden")
    {
        parent::__construct(
            "Forbidden",
            "forbidden",
            403,
            $message
        );
    }
}
