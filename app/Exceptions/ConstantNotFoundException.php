<?php

namespace App\Exceptions;


use Throwable;

class ConstantNotFoundException extends \Exception
{
    public function __construct(string $message = "Constant not found!", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}