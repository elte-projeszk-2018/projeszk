<?php

namespace App\Events;

use App\Models\Conversation;
use App\Models\Message;
use App\User;
use Event;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class MessageSavedEvent extends Event implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /** @var string */
    public $message;
    /** @var string[] */
    public $emails;
    /** @var string */
    public $sender;

    /**
     * MessageSavedEvent constructor.
     * @param Message $message
     * @param Conversation $conversation
     * @param User $owner
     */
    public function __construct(Message $message, Conversation $conversation, User $owner)
    {
        $this->message = $message->message;
        $this->emails = $conversation->users->pluck('email')->toArray();
        $this->sender = $owner->email;
    }


    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('new_message');
    }

    /**
     * The event's broadcast name.
     *
     * @return string
     */
    public function broadcastAs()
    {
        return 'new_message';
    }
}
