<?php

namespace App\Repositories;

use App\Models\Conversation;

class ConversationRepository
{

    public function save(Conversation $conversation): Conversation
    {
        $conversation->save();
        return $conversation;
    }
}