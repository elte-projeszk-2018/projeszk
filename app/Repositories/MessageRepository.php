<?php

namespace App\Repositories;

use App\Models\Message;

class MessageRepository
{

    public function save(Message $message): Message
    {
        $message->save();
        return $message;
    }
}