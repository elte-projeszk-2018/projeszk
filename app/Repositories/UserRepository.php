<?php

namespace App\Repositories;

use App\User;
use Illuminate\Support\Collection;

class UserRepository
{

    public function findAll(): Collection
    {
        return User::all();
    }

    public function find(int $id): User
    {
        return User::findOrFail($id);
    }

    public function create(User $user)
    {
        $user->save();
        return $user;
    }
}