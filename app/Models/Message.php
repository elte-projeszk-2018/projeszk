<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * @SWG\Definition(
 *      definition="Message",
 *      required={"message"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="message",
 *          description="message",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="owner_id",
 *          description="owner_id",
 *          type="int32"
 *      ),
 *      @SWG\Property(
 *          property="conversation_id",
 *          description="conversation_id",
 *          type="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 * )
 * @SWG\Definition(
 *      definition="MessageResponseDto",
 *      allOf={
 *          @SWG\Schema(ref="#/definitions/Message"),
 *          @SWG\Schema(
 *               @SWG\Property(
 *                   ref="#/definitions/UserResponseDto",
 *                   property="owner"
 *               ),
 *               @SWG\Property(
 *                   ref="#/definitions/Conversation",
 *                   property="conversation"
 *               ),
 *          ),
 *      }
 * )
 * @property int $id
 * @property int|null $owner_id
 * @property int $conversation_id
 * @property string $message
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Conversation $conversation
 * @property-read \App\User|null $owner
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereConversationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Message extends Model
{
    public static $rules = [
        'message' => 'required|string|max:20000',
    ];

    protected $fillable = [
        'message',
    ];

    public function conversation() {
        return $this->belongsTo(Conversation::class);
    }
    
    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }
}
