<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
/**
 * @SWG\Definition (
 *      definition="Conversation",
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="admin_id",
 *          description="admin_id",
 *          type="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 * )
 * @SWG\Definition (
 *      definition="ConversationResponseDto",
 *      allOf={
 *          @SWG\Schema(ref="#/definitions/Conversation"),
 *          @SWG\Schema(
 *              @SWG\Property(
 *                 ref="#/definitions/UserResponseDto",
 *                 property="admin"
 *              ),
 *              @SWG\Property(
 *                 property="users",
 *                 type="array",
 *                 @SWG\Items(
 *                     ref="#/definitions/UserResponseDto"
 *                 )
 *              ),
 *              @SWG\Property(
 *                 property="messages",
 *                 type="array",
 *                 @SWG\Items(
 *                     ref="#/definitions/Message"
 *                 )
 *              ),
 *          ),
 *      }
 * )
 * @property int $id
 * @property int $admin_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\User $admin
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Message[] $messages
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conversation whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conversation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conversation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conversation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Conversation extends Model
{
    public static $rules = [
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    public function users() {
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    public function messages() {
        return $this->hasMany(Message::class)->orderBy('id', 'desc');
    }

    public function admin()
    {
        return $this->belongsTo(User::class, 'admin_id');
    }

}
