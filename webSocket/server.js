var server = require('http').Server();
var io = require('socket.io')(server);
var Redis = require('ioredis');
var redis = new Redis(6379, 'redis');

var logedInClients = [];

io.on('connection', (client) => {
    console.log('new client');

    client.on('userLogin', (email) => {
        notInLogedInClientsList = logedInClients.every(function (_client) {
            return _client.email !== email;
        });
        if (notInLogedInClientsList) {
            client.email = email;
            logedInClients.push(client);
            console.log('client logged in', email);
        }
    });

    client.on('disconnect', (client) => {
        console.log('client left');
        if (logedInClients.includes(client)) {
            logedInClients.splice(logedInClients.indexOf(client), 1);
        }
    });
});


redis.subscribe('new_message', function (err, count) {
    console.log('subscribed...');
});

redis.on('message', function (channel, message) {
    // {
    //   "event":"new_message",
    //   "data":{
    //       "message":"Lófasz",
    //       "emails":[
    //           "user1@example.com",
    //           "user2@example.com"
    //       ],
    //       "sender":"user1@example.com",
    //       "socket":null
    //   },
    //   "socket":null
    // }
    // message = JSON.parse(message);
    // io.emit(message.event, message.data);

    console.log('new message from the server');
    
    logedInClients.forEach(function (_client) {
        if (message.data.emails.includes(client)) {
            client.emit('userGotNewMail', null);
            console.log('sendt signal to clent');
        }
    });
});

server.listen(5000);
console.log("Listening...");