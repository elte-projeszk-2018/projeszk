import { authHeader, config } from '../_helpers';

export const chatService = {
    getConversations,
    getMessages,
    sendMessage,
    addUserToConversation,
    deleteUserFromConversation,
    createConversation
};

function getConversations() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(config.apiUrl + config.conversationsEndPoint, requestOptions).then(handleResponse, handleError).then(conversations => {
        return conversations;
    });
}

function getMessages(conversation_id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(config.apiUrl + config.conversationsEndPoint + '/' + conversation_id + '/messages', requestOptions).then(handleResponse, handleError).then(messages => {
        return messages;
    });
}

function sendMessage(text_field, conversation_id) {
    const requestOptions = {
        method: 'POST',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify({message: text_field})
    };

    return fetch(config.apiUrl + config.conversationsEndPoint + '/' + conversation_id + '/messages', requestOptions).then(handleResponse, handleError);
}

function addUserToConversation(id, conversation_id) {
    const requestOptions = {
        method: 'POST',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify({user_id: id})
    };

    return fetch(config.apiUrl + config.conversationsEndPoint + '/' + conversation_id + '/user', requestOptions).then(handleResponse, handleError);
}

function deleteUserFromConversation(id, conversation_id) {
    const requestOptions = {
        method: 'DELETE',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(id)
    };

    return fetch(config.apiUrl + config.conversationsEndPoint + '/' + conversation_id + '/user/' + id, requestOptions).then(handleResponse, handleError);
}

function createConversation(user_ids) {
    const requestOptions = {
        method: 'POST',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify({
            user_ids: user_ids})
    };

    return fetch(config.apiUrl + config.conversationsEndPoint, requestOptions).then(handleResponse, handleError);
}

function handleResponse(response) {
    return new Promise((resolve, reject) => {
        if (response.ok) {
            // return json if it was returned in the response
            var contentType = response.headers.get("content-type");
            if (contentType && contentType.includes("application/json")) {
                response.json().then(json => resolve(json));
            } else {
                resolve();
            }
        } else {
            // return error message from response body
            response.text().then(text => reject(text));
        }
    });
}

function handleError(error) {
    return Promise.reject(error && error.message);
}