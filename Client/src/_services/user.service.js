import { authHeader, config } from '../_helpers';
import { localStorageActions } from '../_actions/localStorage.actions.js';
import { ws } from '../_services/webSocketClient.js';


export const userService = {
    login,
    logout,
    register,
    delete: _delete,
    getUsers,
    updateUser,
    getUser,
    updatePassword
};

function login(email, password) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ email, password })
    };

    return fetch(config.apiUrl + config.authEndPoint + '/login', requestOptions)
        .then(handleResponse, handleError)
        .then(result => {
            // login successful if there's a jwt token in the response
            if (result && result.data.access_token) {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorageActions.setItem('data', JSON.stringify(result.data));
                ws.emit('userLogin', email);
            }

            return result.data;
        });
}

function logout() {
    const requestOptions = {
        method: 'POST',
        headers: authHeader()
    };

    return fetch(config.apiUrl + config.authEndPoint + '/logout', requestOptions)
        .then(handleResponse, handleError)
        .then(() => localStorageActions.removeItem('data'));
}

function register(user) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    };

    return fetch(config.apiUrl + config.userEndPoint, requestOptions).then(handleResponse, handleError);
}

function getUsers() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(config.apiUrl + config.userEndPoint, requestOptions).then(handleResponse, handleError).then(users => {
        return users;
    });
}

function getUser() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(config.apiUrl + config.authEndPoint + "/me", requestOptions).then(handleResponse, handleError).then(res => {
        return res.data;
    });
}

function updateUser(user) {
    const requestOptions = {
        method: 'PUT',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    };

    return fetch(config.apiUrl + config.userEndPoint, requestOptions).then(handleResponse, handleError);
}

function updatePassword(old_password, new_password) {
    const requestOptions = {
        method: 'PUT',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify({old_password, new_password})
    };

    return fetch(config.apiUrl + config.userEndPoint + '/change-password', requestOptions).then(handleResponse, handleError);
}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete() {
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader()
    };

    return fetch(config.apiUrl + config.userEndPoint, requestOptions).then(handleResponse, handleError);
}

function handleResponse(response) {
    return new Promise((resolve, reject) => {
        if (response.ok) {
            // return json if it was returned in the response
            var contentType = response.headers.get("content-type");
            if (contentType && contentType.includes("application/json")) {
                response.json().then(json => resolve(json));
            } else {
                resolve();
            }
        } else {
            // return error message from response body
            response.text().then(text => reject(text));
        }
    });
}

function handleError(error) {
    return Promise.reject(error && error.message);
}