import React from 'react';
import { Router, Route, Link } from "react-router-dom";
import { connect } from 'react-redux';
import { LoginPage } from '../LoginPage'
import { history } from '../_helpers';
import { alertActions, requestActions } from '../_actions';
import { PrivateRoute, MenuComponent } from '../_components'
import { RegisterPage } from '../RegisterPage/RegisterPage';
import { Modal, Button } from 'react-bootstrap';
import { userService } from '../_services';
import { HomePage } from '../HomePage';
import { EditUserProfile } from '../EditUserProfile/EditUserProfile';

class App extends React.Component {
  constructor(props) {
    super(props);

    const { dispatch } = this.props;
    history.listen((location, action) => {
        // clear alert on location change
        dispatch(alertActions.clear());
    });
  }

  render() {
    return(  
      <Router history={history}>
        <div>
          <PrivateRoute exact path="/" component={HomePage} />
          <Route path="/login" component={LoginPage} />
          <Route path="/register" component={RegisterPage} />
          <Route path="/edit-profile" component={EditUserProfile} />
        </div>
      </Router>
    );
  }
}

function mapStateToProps(state) {
    const { alert } = state;
    return {
        alert
    };
}

const connectedApp = connect(mapStateToProps)(App);
export { connectedApp as App }; 