import React from 'react';
import { Navbar, Nav, NavItem} from "react-bootstrap";
import { Link } from 'react-router-dom';
import { LinkContainer, IndexLinkContainer } from 'react-router-bootstrap';

const NavLink = ({ exact, to, eventKey, children }) =>
  <LinkContainer exact={exact} to={to} eventKey={eventKey}>
    <NavItem>{children}</NavItem>
  </LinkContainer>;

class MenuComponent extends React.Component 
{
    constructor(props){
        super(props);
    }

    render(){
        return(
            <Navbar collapseOnSelect>
                <Navbar.Header>
                    <Navbar.Brand>
                        Chat
                    </Navbar.Brand>
                    <Navbar.Toggle />
                </Navbar.Header>
                <Navbar.Collapse>
                    <Nav>
                        <NavLink exact eventKey={1} to={"/"}>
                            Home                       
                        </NavLink>
                    </Nav>
                    <Nav pullRight>
                        <NavLink exact eventKey={1} to={"/login"}>
                            Logout              
                        </NavLink>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}

export {MenuComponent as MenuComponent};
