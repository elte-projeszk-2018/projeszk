import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import { localStorageActions } from '../_actions/localStorage.actions.js';

export const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        localStorageActions.getItem('data')
            ? <Component {...props} />
            : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
    )} />
)