import chai, { expect } from 'chai';

import { alert } from '../../_reducers/alert.reducer';
import { alertConstants } from '../../_constants';

describe('_reducers', () => {

    describe('alert.reducer.js', () => {
        const state = {};
        const action = {
            type: null
        };

        it('returns success right', () => {
            const locAction = {
                type: alertConstants.SUCCESS,
                message: 'success'
            };
            const result = alert(state, locAction);
            expect(result.type).to.equal('alert-success');
            expect(result.message).to.equal('success');
        });

        it('returns error right', () => {
            const locAction = {
                type: alertConstants.ERROR,
                message: 'danger'
            };
            const result = alert(state, locAction);
            expect(result.type).to.equal('alert-danger');
            expect(result.message).to.equal('danger');
        });

        it('returns clear right', () => {
            const locAction = {
                type: alertConstants.CLEAR,
            };
            const result = alert(state, locAction);
            expect(result).to.empty;
        });

        it('returns default right', () => {
            const locState = {
                state: 'state'
            }
            const result = alert(locState, action);
            expect(result.state).to.equal('state');
        });
    });
});