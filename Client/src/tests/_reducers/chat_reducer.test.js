import chai, { expect } from 'chai';

import { chat } from '../../_reducers/chat.reducer';
import { chatConstants } from '../../_constants';

describe('_reducers', () => {

    describe('chat.reducer.js', () => {
        const state = {};
        const action = {};

        it('returns get_conversaitions_requesr right with empty state', () => {
            const locAction = {
                type: chatConstants.GET_CONVERSATIONS_REQUEST
            };
            const result = chat(state, locAction);
            expect(result).to.empty;
        });

        it('returns get_conversaitions_requesr right with set state', () => {
            const locState = {
                state: 'state'
            }
            const locAction = {
                type: chatConstants.GET_CONVERSATIONS_REQUEST
            };
            const result = chat(locState, locAction);
            expect(result.state).to.equal('state');
        });

        it('returns get_conversaitions_success right', () => {
            const locState = {
                state: 'state'
            }
            const locAction = {
                type: chatConstants.GET_CONVERSATIONS_SUCCESS,
                conversations: 'conv'
            };
            const result = chat(locState, locAction);
            expect(result.conversations).to.equal('conv');
            expect(result.state).to.equal('state');
        });

        it('returns get_conversaitions_failed right', () => {
            const locState = {
                state: 'state'
            }
            const locAction = {
                type: chatConstants.GET_CONVERSATIONS_FAILED,
                error: 'error'
            };
            const result = chat(locState, locAction);
            expect(result.conversations).to.equal('error');
            expect(result.state).to.equal('state');
        });

        it('returns get_messagess_request right', () => {
            const locState = {
                state: 'state'
            }
            const locAction = {
                type: chatConstants.GET_MESSAGES_REQUEST,
                id: 1
            };
            const result = chat(locState, locAction);
            expect(result.active_conversation_id).to.equal(1);
            expect(result.state).to.equal('state');
        });

        it('returns get_messagess_success right', () => {
            const locState = {
                state: 'state'
            }
            const locAction = {
                type: chatConstants.GET_MESSAGES_SUCCESS,
                messages: 'message'
            };
            const result = chat(locState, locAction);
            expect(result.messages).to.equal('message');
            expect(result.state).to.equal('state');
        });

        it('returns get_messagess_failed right', () => {
            const locState = {
                state: 'state'
            }
            const locAction = {
                type: chatConstants.GET_MESSAGES_FAILED,
                error: 'error'
            };
            const result = chat(locState, locAction);
            expect(result.messages).to.equal('error');
            expect(result.state).to.equal('state');
        });

        it('returns default right', () => {
            const locState = {
                state: 'state'
            }
            const result = chat(locState, action);
            expect(result.state).to.equal('state');
        });
    });
})