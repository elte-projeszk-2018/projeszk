import chai, { expect } from 'chai';

import { registration } from '../../_reducers/registration.reducer';
import { userConstants } from '../../_constants';

describe('_reducers', () => {

    describe('registration.reducer.js', () => {
        const state = {};
        const action = {
            type: null
        };

        it('returns register_request right', () => {
            const locAction = {
                type: userConstants.REGISTER_REQUEST,
            };
            const result = registration(state, locAction);
            expect(result.registering).to.equal(true);
        });

        it('returns register_succeess right', () => {
            const locAction = {
                type: userConstants.REGISTER_SUCCESS,
            };
            const result = registration(state, locAction);
            expect(result.registering).to.equal(false);
        });

        it('returns register_failture right', () => {
            const locAction = {
                type: userConstants.REGISTER_FAILURE,
            };
            const result = registration(state, locAction);
            expect(result.registering).to.equal(false);
        });

        it('returns default right', () => {
            const locState = {
                state: 'state'
            }
            const result = registration(locState, action);
            expect(result.state).to.equal('state');
        });
    });
});