import chai, { expect } from 'chai';

import { user } from '../../_reducers/user.reducer';
import { userConstants } from '../../_constants';

describe('_reducers', () => {

    describe('user.reducer.js', () => {
        const state = {};
        const action = {
            type: null
        };

        it('returns gest_user right', () => {
            const locAction = {
                type: userConstants.GETUSER_REQUEST,
                message: 'success'
            };
            const result = user(state, locAction);
            expect(result.loading).to.equal(true);
        });

        it('returns gestuser_success right', () => {
            const locAction = {
                type: userConstants.GETUSER_SUCCESS,
                user: 'user'
            };
            const result = user(state, locAction);
            expect(result.user).to.equal('user');
        });

        it('returns guestuser_failture right', () => {
            const locAction = {
                type: userConstants.GETUSER_FAILURE,
                error: 'error'
            };
            const result = user(state, locAction);
            expect(result.error).to.equal('error');
        });

        it('returns delete_request right', () => {
            const locAction = {
                type: userConstants.DELETE_REQUEST,
            };
            const result = user(state, locAction);
            expect(result.deleting).to.equal(true);
        });

        it('returns delete_success right', () => {
            const locAction = {
                type: userConstants.DELETE_SUCCESS,
            };
            const result = user(state, locAction);
            expect(result).to.empty;
        });

        it('returns delete_failture right', () => {
            const locAction = {
                type: userConstants.DELETE_FAILURE,
            };
            const result = user(state, locAction);
            expect(result).to.empty;
        });

        // it('returns history_request right', () => {
        //     const locAction = {
        //         type: userConstants.HISTORY_REQUEST,
        //     };
        //     const result = user(state, locAction);
        //     expect(result).to.empty;
        // });

        // it('returns history_success right', () => {
        //     const locAction = {
        //         type: userConstants.HISTORY_SUCCESS,
        //         history: 'history'
        //     };
        //     const result = user(state, locAction);
        //     expect(result).to.equal('history');
        // });

        // it('returns history_failture right', () => {
        //     const locAction = {
        //         type: userConstants.HISTORY_FAILURE,
        //         error: 'error'
        //     };
        //     const result = user(state, locAction);
        //     expect(result.error).to.equal('error');
        // });

        it('returns get_users_request right', () => {
            const locAction = {
                type: userConstants.GET_USERS_REQUEST,
            };
            const result = user(state, locAction);
            expect(result).to.empty;
        });

        it('returns get_users_success right', () => {
            const locAction = {
                type: userConstants.GET_USERS_SUCCESS,
                users: 'users'
            };
            const result = user(state, locAction);
            expect(result.users).to.equal('users');
        });

        it('returns get_users_failed right', () => {
            const locAction = {
                type: userConstants.GET_USERS_FAILED,
                error: 'error'
            };
            const result = user(state, locAction);
            expect(result.error).to.equal('error');
        });

        it('returns default right', () => {
            const locState = {
                state: 'state'
            };
            const result = user(locState, action);
            expect(result.state).to.equal('state');
        });
    });
});