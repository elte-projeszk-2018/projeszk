import chai, { expect } from 'chai';

import { alertActions } from '../_actions/alert.actions';
import { alertConstants } from '../_constants';

describe('_actions', () => {

    describe('alert.action.js', () => {

        it('returns success function right', () => {
            const result = alertActions.success('this_success');
            expect(result.type).to.equal(alertConstants.SUCCESS);
            expect(result.message).to.equal('this_success');
        });

        it('returns error function right', () => {
            const result = alertActions.error('this_error');
            expect(result.type).to.equal(alertConstants.ERROR);
            expect(result.message).to.equal('this_error');
        });

        it('returns clear function right', () => {
            const result = alertActions.clear();
            expect(result.type).to.equal(alertConstants.CLEAR);
        });
    })
});