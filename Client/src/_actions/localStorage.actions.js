export const localStorageActions = {
    getItem,
    setItem,
    removeItem
};

function getItem(type) {
    return localStorage.getItem(type);
}

function setItem(type, data) {
    localStorage.setItem(type, data)
}

function removeItem(type) {
    localStorage.removeItem(type);
}