import { chatConstants } from '../_constants';
import { chatService } from '../_services';
import { alertActions } from './';
import { history } from '../_helpers';

export const chatActions = {
    getConversations,
    getMessages,
    sendMessage,
    addUserToConversation,
    deleteUserFromConversation,
    createConversation
};

function getConversations() {
    return dispatch => {
        dispatch(request());

        chatService.getConversations()
            .then(
                conversations => { 
                    dispatch(success(conversations));
                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error(error.message));
                }
            );
    };

    function request() { return { type: chatConstants.GET_CONVERSATIONS_REQUEST } }
    function success(conversations) { return { type: chatConstants.GET_CONVERSATIONS_SUCCESS, conversations } }
    function failure(error) { return { type: chatConstants.GET_CONVERSATIONS_FAILED, error } }
}

function getMessages(conversation) {
    return dispatch => {
        dispatch(request(conversation.id));
        dispatch(success(conversation.messages));
    };

    function request(id) { return { type: chatConstants.GET_MESSAGES_REQUEST, id } }
    function success(messages) { return { type: chatConstants.GET_MESSAGES_SUCCESS, messages } }
    function failure(error) { return { type: chatConstants.GET_MESSAGES_FAILED, error } }
}

function sendMessage(text_field, conversation_id) {
    return dispatch => {
        dispatch(request(text_field));

        chatService.sendMessage(text_field, conversation_id)
            .then(
                () => { 
                    dispatch(success(text_field));
                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error(error.message));
                }
            );
    };

    function request(text_field) { return { type: chatConstants.POST_MESSAGES_REQUEST, text_field } }
    function success(text_field) { return { type: chatConstants.POST_MESSAGES_SUCCESS, text_field } }
    function failure(error) { return { type: chatConstants.POST_MESSAGES_FAILED, error } }
}

function addUserToConversation(user_id, conversation_id) {
    return dispatch => {
        dispatch(request(user_id));

        chatService.addUserToConversation(user_id, conversation_id)
            .then(
                () => { 
                    dispatch(success(user_id));
                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error(error.message));
                }
            );
    };

    function request(user_id) { return { type: chatConstants.ADD_USER_TO_CONVERSATION_REQUEST, user_id } }
    function success(user_id) { return { type: chatConstants.ADD_USER_TO_CONVERSATION_SUCCESS, user_id } }
    function failure(error) { return { type: chatConstants.ADD_USER_TO_CONVERSATION_FAILED, error } }
}

function deleteUserFromConversation(user_id, conversation_id) {
    return dispatch => {
        dispatch(request(user_id));

        chatService.deleteUserFromConversation(user_id, conversation_id)
            .then(
                () => { 
                    dispatch(success(user_id));
                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error(error.message));
                }
            );
    };

    function request(user_id) { return { type: chatConstants.DELETE_USER_FROM_CONVERSATION_REQUEST, user_id } }
    function success(user_id) { return { type: chatConstants.DELETE_USER_FROM_CONVERSATION_SUCCESS, user_id } }
    function failure(error) { return { type: chatConstants.DELETE_USER_FROM_CONVERSATION_FAILED, error } }
}

function createConversation(user_ids) {
    return dispatch => {
        dispatch(request(user_ids));

        chatService.createConversation(user_ids)
            .then(
                () => { 
                    dispatch(success(user_ids));
                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error(error.message));
                }
            );
    };

    function request(user_ids) { return { type: chatConstants.POST_CONVERSATIONS_REQUEST, user_ids } }
    function success(user_ids) { return { type: chatConstants.POST_CONVERSATIONS_SUCCESS, user_ids } }
    function failure(error) { return { type: chatConstants.POST_CONVERSATIONS_FAILED, error } }
}