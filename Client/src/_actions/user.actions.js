import { userConstants } from '../_constants';
import { userService } from '../_services';
import { alertActions } from './';
import { history } from '../_helpers';

export const userActions = {
    login,
    logout,
    register,
    delete: _delete,
    getUsers,
    updateUser,
    getUser,
    updatePassword
};

function login(email, password) {
    return dispatch => {
        dispatch(request({ email }));

        userService.login(email, password)
            .then(
                data => { 
                    dispatch(success(data.user));
                    history.push('/');
                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error("Email address or password is incorrect!"));
                }
            );
    };

    function request(user) { return { type: userConstants.LOGIN_REQUEST, user } }
    function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
    function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
}

function logout() {
    userService.logout();  

    return { type: userConstants.LOGOUT };
}

function register(user) {
    return dispatch => {
        dispatch(request(user));

        userService.register(user)
            .then(
                () => { 
                    dispatch(success());
                    history.push('/login');
                    dispatch(alertActions.success('Registration successful!'));
                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error("Username or email address is already taken!"));
                }
            );
    };

    function request(user) { return { type: userConstants.REGISTER_REQUEST, user } }
    function success(user) { return { type: userConstants.REGISTER_SUCCESS, user } }
    function failure(error) { return { type: userConstants.REGISTER_FAILURE, error } }
}

function getUser() {
    return dispatch => {
        dispatch(request());

        userService.getUser()
            .then(
                user => dispatch(success(user)),
                error => dispatch(failure(error))
            );
    };

    function request() { return { type: userConstants.GETUSER_REQUEST } }
    function success(user) { return { type: userConstants.GETUSER_SUCCESS, user } }
    function failure(error) { return { type: userConstants.GETUSER_FAILURE, error } }
}

function updateUser(user) {
    return dispatch => {
        dispatch(request(user));

        userService.updateUser(user)
            .then(
                user => {
                    dispatch(success(user));
                    dispatch(getUser());
                    dispatch(alertActions.success('User profile saved!'));
                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error(error))
                }
            );
    };

    function request(user) { return { type: userConstants.UPDATEUSER_REQUEST, user } }
    function success(user) { return { type: userConstants.UPDATEUSER_SUCCESS, user } }
    function failure(error) { return { type: userConstants.UPDATEUSER_FAILURE, error } }
}

function updatePassword(old_pass, new_pass) {
    return dispatch => {
        dispatch(request());

        userService.updatePassword(old_pass, new_pass)
            .then(
                user => {
                    dispatch(success());
                    dispatch(getUser());
                    dispatch(alertActions.success('Password saved!'));
                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error('Old password is incorrect!'));
                }
            );
    };

    function request() { return { type: userConstants.UPDATEUSERPASS_REQUEST } }
    function success() { return { type: userConstants.UPDATEUSERPASS_SUCCESS } }
    function failure() { return { type: userConstants.UPDATEUSERPASS_FAILURE } }
}

function _delete() {
    return dispatch => {
        dispatch(request());

        userService.delete()
            .then(
                () => { 
                    dispatch(success());
                    history.push('/login');
                    dispatch(alertActions.success('User profile deleted successfully!'));
                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error(error))
                }
            );
    };

    function request() { return { type: userConstants.DELETE_REQUEST } }
    function success() { return { type: userConstants.DELETE_SUCCESS } }
    function failure(error) { return { type: userConstants.DELETE_FAILURE, error } }
}

function getUsers() {
    return dispatch => {
        dispatch(request());

        userService.getUsers()
            .then(
                users => { 
                    dispatch(success(users));
                    console.log(users);
                },
                error => {
                    dispatch(failure(error));
                }
            );
    };

    function request() { return { type: userConstants.GET_USERS_REQUEST } }
    function success(users) { return { type: userConstants.GET_USERS_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GET_USERS_FAILED, error } }
}

function updateUser(user) {
    return dispatch => {
        dispatch(request(user));

        userService.updateUser(user)
            .then(
                user => {
                    dispatch(success(user));
                    dispatch(getUser());
                    dispatch(alertActions.success('User profile saved!'));
                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error(error))
                }
            );
    };

    function request(user) { return { type: userConstants.UPDATEUSER_REQUEST, user } }
    function success(user) { return { type: userConstants.UPDATEUSER_SUCCESS, user } }
    function failure(error) { return { type: userConstants.UPDATEUSER_FAILURE, error } }
}