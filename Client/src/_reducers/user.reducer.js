import { userConstants } from '../_constants';

export function user(state = {}, action) {
  switch (action.type) {
    case userConstants.GETUSER_REQUEST:
      return {
        loading: true
      };
    case userConstants.GETUSER_SUCCESS:
      return {
        user: action.user
      };
    case userConstants.GETUSER_FAILURE:
      return { 
        error: action.error
      };
    case userConstants.DELETE_REQUEST:
      return {
          deleting: true
      };
    case userConstants.DELETE_SUCCESS:
      return {};
    case userConstants.DELETE_FAILURE:
      return {};
    case userConstants.HISTORY_REQUEST:
      return {};
    case userConstants.HISTORY_SUCCESS:
      return {history: action.history};
    case userConstants.HISTORY_FAILURE:
      return {error: action.error};
      case userConstants.GET_USERS_REQUEST:
      return {};
    case userConstants.GET_USERS_SUCCESS:
      return {users: action.users};
    case userConstants.GET_USERS_FAILED:
      return {error: action.error};
    default:
      return state
  }
}