import { userConstants } from '../_constants';

import { localStorageActions } from '../_actions/localStorage.actions.js';

let data = JSON.parse(localStorageActions.getItem('data'));
const initialState = data ? { loggedIn: true, user: data.user } : {};

export function authentication(state = initialState, action) {
  switch (action.type) {
    case userConstants.LOGIN_REQUEST:
      return {
        loggingIn: true,
        user: action.user
      };
    case userConstants.LOGIN_SUCCESS:
      return {
        loggedIn: true,
        user: action.user
      };
    case userConstants.LOGIN_FAILURE:
      return {};
    case userConstants.LOGOUT:
      return {};
    default:
      return state
  }
}