import { chatConstants } from '../_constants';

export function chat(state = {}, action) {
  switch (action.type) {
    case chatConstants.GET_CONVERSATIONS_REQUEST:
      return state;
    case chatConstants.GET_CONVERSATIONS_SUCCESS:
      return Object.assign({}, state, {conversations: action.conversations});
    case chatConstants.GET_CONVERSATIONS_FAILED:
      return Object.assign({}, state, {conversations: action.error});
    case chatConstants.GET_MESSAGES_REQUEST:
      return Object.assign({}, state, {active_conversation_id: action.id});
    case chatConstants.GET_MESSAGES_SUCCESS:
      return Object.assign({}, state, {messages: action.messages});
    case chatConstants.GET_MESSAGES_FAILED:
      return Object.assign({}, state, {messages: action.error});
    default:
      return state
  }
}