import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { userActions } from '../_actions';
import { Col, FormGroup, ControlLabel, FormControl, ButtonGroup, Button, Alert} from 'react-bootstrap'

class LoginPage extends React.Component {
    constructor(props) {
        super(props);

        // reset login status
        this.props.dispatch(userActions.logout());

        this.state = {
            email: '',
            password: '',
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { email, password } = this.state;
        const { dispatch } = this.props;
        if (email && password) {
            dispatch(userActions.login(email, password));
        }
    }

    render() {
        const { loggingIn, alert } = this.props;
        const { email, password, submitted } = this.state;
        return (
            <div className="container">
                {loggingIn && <div className="waiting-status"></div>}
                <Col md={6} mdOffset={3}>
                    <h2>Login</h2>
                    <form name="form">
                        <FormGroup controlId="email" validationState={null}>
                            <ControlLabel>Email</ControlLabel>
                            <FormControl type="text" name="email" value={email} onChange={this.handleChange} />
                        </FormGroup>
                        <FormGroup controlId="password" validationState={null}>
                            <ControlLabel>Password</ControlLabel>
                            <FormControl type="password" name="password" value={password} onChange={this.handleChange}/>
                        </FormGroup>
                        {alert && alert.type == "alert-danger" &&
                            <Alert bsStyle="danger">
                            <p>
                                {alert.message}
                            </p>
                            </Alert>
                        }
                        {alert && alert.type == "alert-success" &&
                            <Alert bsStyle="success">
                            <p>
                                {alert.message}
                            </p>
                            </Alert>
                        }
                        <ButtonGroup justified>
                            <Button onClick={this.handleSubmit} href="#" bsStyle="primary">Login</Button>
                            <Link to="/register" className="btn btn-default">Register</Link>
                        </ButtonGroup>
                    </form>
                </Col>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    const { alert } = state;
    return {
        loggingIn,
        alert
    };
}

const connectedLoginPage = connect(mapStateToProps)(LoginPage);
export { connectedLoginPage as LoginPage }; 