import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { MenuComponent } from '../_components/MenuComponent'
import { userActions, alertActions } from '../_actions';
import { Col, FormGroup, FormControl, ControlLabel, Button, ButtonGroup, Collapse, Row, Alert, Modal, ModalBody, ModalFooter } from 'react-bootstrap'
import { authHeader, config } from '../_helpers';

class EditUserProfile extends React.Component {   
    constructor(props)
    {
        super(props);

        this.state = {
            open : false
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleDeleteUser = this.handleDeleteUser.bind(this);
        this.changePassword = this.changePassword.bind(this);
        this.updatePassword = this.updatePassword.bind(this);
    }

    componentWillReceiveProps(props) {
        const {user, alert} = props;

        if(alert.type != 'alert-danger')
            this.setState(
            {
                user: 
                    {
                        ...user,
                        confirm_password: '',
                        old_password: ''
                    },
                open: false
            });
    }

    componentWillMount() {
        this.props.dispatch(userActions.getUser());
    }

    handleChange(event) {
        const { name, value } = event.target;
        const { user } = this.state;

        this.setState({
            user: {
                ...user,
                [name]: value
            },
            open: this.state.open
        });
    }

    handleSubmit() {
        if(!this.validateForm()) return;

        this.setState({ submitted: true });
        const { user } = this.state;
        const { dispatch } = this.props;
        if (user.email) {
            dispatch(userActions.updateUser(user));
        }
    }

    handleDeleteUser() {
        this.props.dispatch(userActions.delete());
    }

    changePassword() {
        this.setState({
            user: {
                ...this.state.user,
                password: '',
                confirm_password: '',
                old_password: ''
            },
            open: !this.state.open
        })
    }

    updatePassword() {
        if (this.validatePassword() == 'success' && this.validateConfPassword() == 'success') {
            this.props.dispatch(userActions.updatePassword(this.state.user.old_password, this.state.user.password));

            this.changePassword();
        }
    }

    validateEmail() {
        const { user } = this.state;

        if(!user) return null;

        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if(user.email != '' && re.test(user.email)) return 'success';
        else return 'error';
    }

    validatePassword() {
        const { user } = this.state;

        if(!user) return null;

        if(user.password == '' || user.password == null) return null;
        if(user.password.length < 5) return 'error';

        return 'success';
    }

    validateConfPassword() {
        const { user } = this.state;

        if(!user) return null;

        if(user.password == '' || this.validatePassword() != 'success') return null;
        if(user.password != '' && user.password != user.confirm_password) return 'error';

        return 'success';
    }


    validateFirstName() {
        const { user } = this.state;

        if(user.first_name == '') return null;

        return 'success';
    }

    validateLastName() {
        const { user } = this.state;

        if(user.last_name == '') return null;

        return 'success';
    }

    validateForm() {
        if (
            (this.validateEmail() == 'success' && this.validateFirstName() && this.validateLastName() && this.validatePassword() == null && this.validateConfPassword() == null)
            ||
            (this.validateEmail() == 'success' && this.validateFirstName() && this.validateLastName() && this.validatePassword() == 'success' && this.validateConfPassword() == 'success')
        ) {
            return true;
        } else {
            return false;
        }
    }

    render() {
        const { user } = this.state;
        const { alert } = this.props;

        return (
            <div className="container">
            {(!user || user === undefined) && <div className="waiting-status"></div>}
            {user &&
                <Col md={6} mdOffset={3}>
                    <form name="form">
                        <FormGroup controlId="first_name" validationState={this.validateFirstName()}>
                            <ControlLabel>First name</ControlLabel>
                            <FormControl type="text" name="first_name" value={(user.first_name ? user.first_name : "")} onChange={this.handleChange}/>
                        </FormGroup>
                        <FormGroup controlId="last_name" validationState={this.validateLastName()}>
                            <ControlLabel>Last name</ControlLabel>
                            <FormControl type="text" name="last_name" value={(user.last_name ? user.last_name : "")} onChange={this.handleChange}/>
                        </FormGroup>
                        <FormGroup controlId="email" validationState={this.validateEmail()}>
                            <ControlLabel>Email</ControlLabel>
                            <FormControl type="text" name="email" value={(user.email ? user.email : "")} onChange={this.handleChange} readOnly/>
                        </FormGroup>
                        <FormGroup>
                            <Button onClick={this.changePassword}>
                                Change password
                            </Button>
                        </FormGroup>
                        <Modal
                        show={this.state.open}
                        onHide={this.changePassword}
                        container={this}
                        aria-labelledby="modal-title"
                        >
                            <Modal.Header closeButton>
                                <Modal.Title id="modal-title">
                                Change password
                                </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                            <div>
                                <FormGroup controlId="old_password" validationState={null}>
                                    <ControlLabel>Old password</ControlLabel>
                                    <FormControl type="password" name="old_password" value={(user.old_password ? user.old_password : "")} onChange={this.handleChange}/>
                                </FormGroup>
                                <FormGroup controlId="password" validationState={this.validatePassword()}>
                                    <ControlLabel>Password</ControlLabel>
                                    <FormControl type="password" name="password" value={(user.password ? user.password : "")} onChange={this.handleChange}/>
                                </FormGroup>
                                <FormGroup controlId="confirm_password" validationState={this.validateConfPassword()}>
                                    <ControlLabel>Confirm password</ControlLabel>
                                    <FormControl type="password" name="confirm_password" value={(user.confirm_password ? user.confirm_password : "")} onChange={this.handleChange}/>
                                </FormGroup>
                            </div>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button onClick={this.updatePassword}>Send</Button>
                                <Button onClick={this.changePassword}>Close</Button>
                            </Modal.Footer>
                        </Modal>
                        {alert && alert.type == "alert-danger" &&
                            <Alert bsStyle="danger">
                            <p>
                                {alert.message}
                            </p>
                            </Alert>
                        }
                        {alert && alert.type == "alert-success" &&
                            <Alert bsStyle="success">
                            <p>
                                {alert.message}
                            </p>
                            </Alert>
                        }
                        <ButtonGroup justified>
                            <Button href="#" bsStyle="primary" onClick={this.handleSubmit}>Save</Button>
                            <Button href="#" onClick={this.handleDeleteUser}>Delete</Button>
                            <Link to="/" className="btn btn-default">Back</Link>
                        </ButtonGroup>                         
                    </form>
                </Col>
            }
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { user } = state.user;
    const { alert } = state;

    return {
        user,
        alert
    };
}

const connectedEditUserProfile = connect(mapStateToProps)(EditUserProfile);
export { connectedEditUserProfile as EditUserProfile };