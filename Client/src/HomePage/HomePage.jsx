import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { userActions, chatActions } from '../_actions';
import { history } from '../_helpers';
import { Col, Row, Button, Alert, ButtonToolbar, FormControl, DropdownButton, Modal, ModalBody, ModalHeader, MenuItem, OverlayTrigger, Tooltip} from 'react-bootstrap'

import { ws } from '../_services/webSocketClient.js';

class HomePage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            searching: false,
            selected: [],
            searchPattern: '',
            addUserToConversationModal: false,
            deleteUserFromConversationModal: false,
            messageToSend: ''
        };

        this.startSearching = this.startSearching.bind(this);
        this.logout = this.logout.bind(this);
        this.setSelected = this.setSelected.bind(this);
        this.setSearchPattern = this.setSearchPattern.bind(this);
        this.openConversation = this.openConversation.bind(this);
        this.editProfile = this.editProfile.bind(this);
        this.handleHide = this.handleHide.bind(this);
        this.addUserToConversationModalShow = this.addUserToConversationModalShow.bind(this);
        this.deleteUserFromConversationModalShow = this.deleteUserFromConversationModalShow.bind(this);
        this.addUserToConversation = this.addUserToConversation.bind(this);
        this.changeMessageToSend = this.changeMessageToSend.bind(this);
        this.sendMessage = this.sendMessage.bind(this);
        this.createConversation = this.createConversation.bind(this);
    }

    startSearching() {
        var selected = [];

        this.props.users.data.forEach((item, index) => {
            selected.push(false);
        })

        this.setState({
            ...this.state,
            searching: !this.state.searching,
            selected: selected
        })
    }

    setSelected(i) {
        const { selected } = this.state;
        selected[i] = !selected[i];

        this.setState({...this.state, selected: selected})
    }

    setSearchPattern(e) {
        this.setState({
            ...this.state,
            searchPattern: e.target.value
        })
    }

    changeMessageToSend(e) {
        this.setState({
            ...this.state,
            messageToSend: e.target.value
        })
    }

    sendMessage() {
        this.props.dispatch(chatActions.sendMessage(this.state.messageToSend, this.props.active_conversation_id));

        this.setState({
            ...this.state,
            messageToSend: ''
        });
    }

    openConversation(conversation) {
        this.props.dispatch(chatActions.getMessages(conversation));
    }

    componentDidMount() {
        this.props.dispatch(userActions.getUsers());
        this.props.dispatch(chatActions.getConversations());
    }

    componentWillUnmonut() {
        ws.removeAllListeners();
    }

    componentDidMount() {
        ws.on('userGotNewMail', () => {
            updateProps();
        });
    }

    updateProps() {
        this.props.dispatch(userActions.getUsers());
        this.props.dispatch(chatActions.getConversations());
        if (this.props.active_conversation_id != -1) this.props.dispatch(chatActions.getMessages(this.props.active_conversation_id));
    }

    componentWillReceiveProps(props) {
        var selected = [];

        if(props.users) {
            props.users.data.forEach((item, index) => {
                selected.push(false);
            })
        }

        this.setState({...this.state, selected: selected, messageToSend: ''});
    }

    logout() {
        history.push('/login');
    }

    editProfile() {
        history.push('/edit-profile');
    }

    handleHide() {
        this.setState({
            ...this.state,
            addUserToConversationModal: false,
            deleteUserFromConversationModal: false
        })
    }

    addUserToConversationModalShow() {
        this.setState({
            ...this.state,
            addUserToConversationModal: true
        });
    }

    deleteUserFromConversationModalShow() {
        this.setState({
            ...this.state,
            deleteUserFromConversationModal: true
        });
    }

    addUserToConversation(user_id) {
        this.props.dispatch(chatActions.addUserToConversation(user_id, this.props.active_conversation_id));
        this.handleHide();
    }

    deleteUserFromConversation(user_id) {
        this.props.dispatch(chatActions.deleteUserFromConversation(user_id, this.props.active_conversation_id));
        this.handleHide();
    }

    createConversation() {
        this.setState({
            ...this.state, searching: false
        });

        var user_ids = [];

        this.state.selected.forEach( (ifSelected, index) => {
            if(ifSelected) user_ids.push(this.props.users.data[index].id);
        });

        this.props.dispatch(chatActions.createConversation(user_ids));
    }

    tooltip(text, id) {
        return (
            <Tooltip id={id}>
              {text}
            </Tooltip>
        )
    }

    render() {
        const { alert, users, conversations, messages } = this.props;

        return (
            <div>
                <Col xs={3} className="conversations-container">
                    <Row className="header">
                        <OverlayTrigger placement="left" overlay={this.tooltip('Search users!', 'search-users-btn')}>
                            <i className="fas fa-plus fa-2x" onClick={this.startSearching}></i>
                        </OverlayTrigger>
                        <ButtonToolbar>
                            <DropdownButton bsSize='small' title="Settings" id="settings-btn">
                                <MenuItem eventKey="1" onClick={this.editProfile}>Edit profile</MenuItem>
                                <MenuItem eventKey="2" onClick={this.logout}>Logout</MenuItem>
                            </DropdownButton>
                        </ButtonToolbar>
                    </Row>
                    {this.state.searching &&
                        <Row className="search-user">
                            <Col className="search-user-input">   
                                <FormControl
                                    type="text"
                                    value={this.state.searchPattern}
                                    placeholder="Search user..."
                                    onChange={this.setSearchPattern}
                                />

                                <Button bsStyle="primary" onClick={this.createConversation}>Create</Button>
                            </Col>
                            <Col>
                                {users && 
                                    users.data.map((user, index) => {
                                        if(this.state.searchPattern != '' && !user.full_name.toLowerCase().includes(this.state.searchPattern.toLowerCase())) return;
                                        if(this.props.user && user.email == this.props.user.email) return;

                                        return (
                                            <div key={index} className="user-container" onClick={() => this.setSelected(index)}>
                                                <Col xs={10}>
                                                    <p className="highlighted">
                                                        {user.full_name}
                                                    </p>
                                                    <p className="sub-text">
                                                        {user.email}
                                                    </p>
                                                </Col>
                                                <Col xs={2}>
                                                    {this.state.selected[index] == true && <i className="fas fa-check fa-2x selected-user"></i>}
                                                </Col>
                                            </div>
                                        )
                                    })
                                }
                            </Col>
                        </Row>
                    }
                    {!this.state.searching &&
                        <Row className="conversations">
                                {conversations &&
                                    conversations.data.map( (conversation,index) => {
                                        return (
                                            <Col xs={12} key={index} 
                                                className={'conversation-container ' + (this.props.active_conversation_id == conversation.id ? 'active' : '')} 
                                                onClick={() => this.openConversation(conversation)}>
                                                <p className="highlighted">
                                                    {conversation.users.map( (user, index) => {
                                                        return user.email + ' ';
                                                    })}
                                                </p>
                                            </Col>
                                        )
                                    })
                                }
                        </Row>
                    }
                </Col>
                <Col xs={9} className="chat-interface-container">
                {messages && 
                    <div>
                        <Row className="header">
                            <OverlayTrigger placement="bottom" overlay={this.tooltip('Add user to conversation!', 'add-user-to-conversation')}>
                                <i className="fas fa-plus fa-2x" onClick={this.addUserToConversationModalShow}></i>
                            </OverlayTrigger>
                            {/* <OverlayTrigger placement="left" overlay={this.tooltip('Delete user from conversation!', 'delete-user-from-conversation')}> 
                                <i className="fas fa-trash fa-2x" onClick={this.deleteUserFromConversationModalShow}></i>
                            </OverlayTrigger> */}
                        </Row>
                        <Row className="messages-container">                        
                                {messages.map( (message, index) => {
                                    return (
                                        <div key={index} className={'message ' + (message.owner_id == this.props.user.id ? 'own' : '')}>
                                            <p className="sub-text">
                                                {message.owner_email}
                                            </p>
                                            <p>
                                                {message.message}
                                            </p>
                                        </div>
                                    )
                                })}                           
                        </Row>
                        <div className="send-message-input">
                            <Col xs={10}>
                                <FormControl
                                    type="text"
                                    value={this.state.messageToSend}
                                    placeholder="Write your text..."
                                    onChange={this.changeMessageToSend}
                                />
                            </Col>
                            <Col xs={2}>
                                <OverlayTrigger placement="top" overlay={this.tooltip('Send message!', 'send-message')}>
                                    <i className="fas fa-comment fa-2x" onClick={this.sendMessage}></i>
                                </OverlayTrigger>
                            </Col>
                        </div>
                    </div>
                }
                </Col>
                <Modal
                show={this.state.addUserToConversationModal}
                onHide={this.handleHide}
                container={this}
                aria-labelledby="modal-title"
                >
                    <Modal.Header closeButton>
                        <Modal.Title id="modal-title">
                         Add user to conversation
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {users && 
                            users.data.map((user, index) => {
                                if(this.props.user && user.email == this.props.user.email) return;

                                return (
                                    <div key={index} className="user-container" onClick={() => this.addUserToConversation(user.id)}>
                                        <Col xs={12}>
                                            <p className="highlighted">
                                                {user.full_name}
                                            </p>
                                            <p className="sub-text">
                                                {user.email}
                                            </p>
                                        </Col>
                                    </div>
                                )
                            })
                        }
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.handleHide}>Close</Button>
                    </Modal.Footer>
                </Modal>
                <Modal
                    show={this.state.deleteUserFromConversationModal}
                    onHide={this.handleHide}
                    container={this}
                    aria-labelledby="delete-modal-title"
                >
                    <Modal.Header closeButton>
                        <Modal.Title id="delete-modal-title">
                        Delete user from conversation
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {users && 
                            users.data.map((user, index) => {
                                if(user.email == this.props.user.email) return;
                                
                                return (
                                    <div key={index} className="user-container" onClick={() => this.deleteUserFromConversation(user.id)}>
                                        <Col xs={12}>
                                            <p className="highlighted">
                                                {user.full_name}
                                            </p>
                                            <p className="sub-text">
                                                {user.email}
                                            </p>
                                        </Col>
                                    </div>
                                )
                            })
                        }
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.handleHide}>Close</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { alert } = state;
    const { users } = state.user;
    const { conversations, messages, active_conversation_id } = state.chat;
    const { user } = state.authentication;
    return {
        alert,
        users,
        conversations: conversations,
        messages: messages,
        active_conversation_id,
        user
    };
}

const connectedHomePage = connect(mapStateToProps)(HomePage);
export { connectedHomePage as HomePage }; 