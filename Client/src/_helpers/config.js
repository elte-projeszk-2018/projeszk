export const config = {
    apiUrl: 'http://127.0.0.1:880/api/',
    userEndPoint: 'users',
    authEndPoint: 'auth',
    conversationsEndPoint: 'conversations'
};