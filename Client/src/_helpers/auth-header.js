
import { localStorageActions } from '../_actions/localStorage.actions.js';

export function authHeader() {
    // return authorization header with jwt token
    let data = JSON.parse(localStorageActions.getItem('data'));

    if (data && data.access_token) {
        return { 'Authorization': 'Bearer ' + data.access_token };
    } else {
        return {};
    }
}