import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { userActions } from '../_actions';
import { Col, FormGroup, ControlLabel, FormControl, ButtonGroup, Button, Alert} from 'react-bootstrap'

class RegisterPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user: {
                password: '',
                first_name: '',
                last_name: '',
                email: '',
                confirm_password: ''
            },
            valid: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        const { name, value } = event.target;
        const { user } = this.state;

        this.setState({
            ...this.state,
            user: {
                ...user,
                [name]: value
            }
        });
    }

    handleSubmit(event) {
        event.preventDefault();

        if(!this.validateForm()) return;

        const { user } = this.state;
        const { dispatch } = this.props;
        if (user.email && user.password) {
            dispatch(userActions.register(user));
        }
    }

    validateEmail() {
        const { user } = this.state;

        if(user.email == '') return null;

        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if(re.test(user.email)) return 'success';
        else return 'error';
    }

    validatePassword() {
        const { user } = this.state;

        if(user.password == '') return null;
        if(user.password.length < 5) return 'error';

        return 'success';
    }

    validateConfPassword() {
        const { user } = this.state;

        if(user.password == '' || this.validatePassword() != 'success') return null;
        if(user.password != '' && user.password != user.confirm_password) return 'error';

        return 'success';
    }

    validateFirstName() {
        const { user } = this.state;

        if(user.first_name == '') return null;

        return 'success';
    }

    validateLastName() {
        const { user } = this.state;

        if(user.last_name == '') return null;

        return 'success';
    }

    validateForm() {
        if (this.validateEmail() == 'success' && 
            this.validatePassword() == 'success' && this.validateConfPassword() == 'success' &&
            this.validateLastName() == 'success' && this.validateFirstName() == 'success') {
            return true;
        } else {
            return false;
        }
    }

    render() {
        const { alert } = this.props;
        const { user } = this.state;

        return (
            <div className="container game-container">
                {this.props.registering && <div className="waiting-status"></div>}
                <Col md={6} mdOffset={3}>
                    <h2>Register</h2>
                    <form name="form">
                        <FormGroup controlId="email" validationState={this.validateEmail()}>
                            <ControlLabel>Email</ControlLabel>
                            <FormControl type="text" name="email" value={user.email} onChange={this.handleChange} />
                        </FormGroup>
                        <FormGroup controlId="first_name" validationState={this.validateFirstName()}>
                            <ControlLabel>First name</ControlLabel>
                            <FormControl type="text" name="first_name" value={user.first_name} onChange={this.handleChange} />
                        </FormGroup>
                        <FormGroup controlId="last_name" validationState={this.validateLastName()}>
                            <ControlLabel>Last name</ControlLabel>
                            <FormControl type="text" name="last_name" value={user.last_name} onChange={this.handleChange} />
                        </FormGroup>
                        <FormGroup controlId="password" validationState={this.validatePassword()}>
                            <ControlLabel>Password</ControlLabel>
                            <FormControl type="password" name="password" value={user.password} onChange={this.handleChange}/>
                        </FormGroup>
                        <FormGroup controlId="confirm_password" validationState={this.validateConfPassword()}>
                            <ControlLabel>Confirm password</ControlLabel>
                            <FormControl type="password" name="confirm_password" value={user.confirm_password} onChange={this.handleChange}/>
                        </FormGroup>
                        {alert && alert.type == "alert-danger" &&
                            <Alert bsStyle="danger">
                            <p>
                                {alert.message}
                            </p>
                            </Alert>
                        }
                        <ButtonGroup justified>
                            <Button onClick={this.handleSubmit} href="#" bsStyle="primary">Register</Button>
                            <Link to="/login" className="btn btn-default">Login</Link>
                        </ButtonGroup>
                    </form>
                </Col>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { alert } = state;
    const {registering} = state.registration;
    return {
        alert,
        registering
    };
}

const connectedRegisterPage = connect(mapStateToProps)(RegisterPage);
export { connectedRegisterPage as RegisterPage };