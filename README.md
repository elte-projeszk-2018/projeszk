# Project name Placeholder :)
Assignment for 5 attendees of ELTE's 2017/18/2 IP-08cPRJG 1 - Projekt eszközök GY. (BSc,08,C)

## Built With

* Backend
    * [PHP](http://php.net/)
    * [Laravel](https://laravel.com/)
    * [Composer](https://getcomposer.org/)
    * [Docker, Docker-compose](https://www.docker.com/)
* Frontend
    * [JavaScript](https://www.javascript.com/)
    * [React](https://reactjs.org/)
    * [Webpack](https://webpack.js.org/)
    * [Socket.io](https://socket.io/)

## Authors

* **Kósa Réka** - [gitlab](https://gitlab.com/forfoxsake), [github](https://github.com)
* **Major Gábor** - [gitlab](https://gitlab.com/majorgabor), [github](https://github.com/majorgabor)
* **Szoboszlai Norbert** - [gitlab](https://gitlab.com/leaderhun), [github](https://github.com/leaderhun)
* **Sevcsik Marcell** - [gitlab](https://gitlab.com/0sewa0), [github](https://github.com/0sewa0)
* **Hete Csaba** - [gitlab](https://gitlab.com/csaba-hete), [github](https://github.com/csabahete)

## Init

Stuff that you need installed:
* [Docker](https://docs.docker.com/install/)
* [Docker-compose](https://docs.docker.com/compose/install/)
* (optional)[GNU make](http://gnuwin32.sourceforge.net/packages/make.htm)

Project initialization:
```bash
# To make the app available on 127.0.0.1:880
# run at the docker directory

make build #or use the commands from Makefile
make start #or use the commands from Makefile

# To generate the swagger documentation, it will be available ./api/documentation
# run at the docker directory after starting the docker.

make swagger #or use the commands from Makefile

```

## Testing

Both the server and the client is tested.
* The php server has functional test made with phpunit.
    * Files: _./tests/..._
    * Its automaticly tested in the **php_docker_build_test** CI job.
* The javascipt(react) client has unit test made with the Mocha test framework.
    * Files: _./Client/src/tests/..._
    * Its automaticly tested in the **react_test** CI job.

## CI
The project uses the built in CI functionality of GitLab.
The configuration can be found in: **\.gitlab-ci.yml** file.
Some jobs have downloadable artifacts that can be used during development or deployment.

The runners(similar to a Jenkins slave) are hosted by GitLab and sometimes fail for some reason.
When a job fails with:
   * Error code: 137 or something similar (a 3 digit code)
   * With message: Error: No such container: 745...

In these cases GitLab is at fault, fortunately you can restart the failed job.

[Pipelines](https://gitlab.com/elte-projeszk-2018/projeszk/pipelines)

* Every pushed commit on feature branches are built and tested. 
* Every pushed commit to the master(mostly merges from feature branches) is built and deployed.
* Testing is skipped on master, because ONLY feature branch merges(via merge request) and CI updates are allowed to be pushed to the master.
    * CI updates are not tested and don't change the product, so runnig the testing jobs is redundent.
    * Commits on feature branches have already been tested, so runnig the testing on master is redundent.

## Extra tools

* [Docker:]((https://www.docker.com/))
    * The application is running in a docker container for easy deployment.
* [Swagger:](https://swagger.io/)
    * The documentation for the php server api is generated with swagger. It creates html site where you can see what calls you can make and you can try them out.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
