FROM app_base

# Install Nginx.
RUN add-apt-repository -y ppa:nginx/stable && \
    apt-get update && apt-get install -y nginx && \
    chown -R www-data:www-data /var/log/nginx && \
    apt-get clean && apt-get autoremove -y

# Let docker monitor the logs
RUN rm /var/log/nginx/error.log && rm /var/log/nginx/access.log && \
    ln -snf /dev/stdout /var/log/nginx/access.log && \
    ln -snf /dev/stdout /var/log/nginx/error.log

# Define mountable directories.
VOLUME ["/etc/nginx/sites-enabled", "/etc/nginx/certs", "/etc/nginx/conf.d", "/var/log/nginx", "/var/www/html"]

# App specific part
VOLUME ["/var/www/html/projeszk"]

COPY ./conf/nginx.conf /etc/nginx/nginx.conf
COPY ./conf/projeszk.conf /etc/nginx/sites-available/default

RUN chown -R www-data:www-data /var/www/html/projeszk

# Define working directory.
WORKDIR /etc/nginx

# Expose ports.
EXPOSE 80
EXPOSE 443

# Define default command.
CMD nginx -g "daemon off;"
