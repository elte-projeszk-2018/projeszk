#!/usr/bin/env bash
# setup container at it's first run (if .env not found)
chown -R www-data /var/www/html/projeszk/

COMPOSER_ARG=""
if [ ! -z $APP_ENVIRONMENT ]; then
    COMPOSER_ARG="--no-dev"
fi

if [ ! -f .env ]; then
    echo "First time PRODUCTION startup, building the application..."
    composer install $COMPOSER_ARG
    cp .env.example .env
    php artisan jwt:secret
    php artisan key:generate

    sed -i 's/^APP_ENV=.*$/APP_ENV=PRODUCTION/' .env
    echo "App is ready to use!"
fi

if [ ! -z $APP_ENVIRONMENT ]; then
    composer update $COMPOSER_ARG
fi

php artisan config:clear
php artisan config:cache

while true; do
    echo "Starting migration..."
    echo "Migrating app DB..."
    php artisan migrate --force
    APP_DB_MIGRATED=$?
    echo "Migrating test DB..."
    php artisan migrate --force --database=testing
    TEST_DB_MIGRATED=$?
    if [ $APP_DB_MIGRATED -eq 0 ] && [ $TEST_DB_MIGRATED -eq 0 ]; then
        break
    else
        echo "Mysql wasn't ready, sleeping..."
    fi
    sleep 5
done

echo "Migration's finished!"

php artisan l5-swagger:generate

echo "Server is ready!"