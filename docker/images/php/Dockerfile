FROM app_base

# Install PHP with FPM and other various commonly used modules, including MySQL client
RUN LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php && \
    apt-get update && apt-get install -y --allow-downgrades --allow-remove-essential --allow-change-held-packages \
    php7.2-bcmath \
    php7.2-bz2 \
    php7.2-cli \
    php7.2-com \
    php7.2-cur \
    php7.2-dev \
    php7.2-fpm \
    php7.2-gd \
    php7.2-gmp \
    php7.2-imap \
    php7.2-intl \
    php7.2-json \
    php7.2-ldap \
    php7.2-mbstring \
    php7.2-mysql \
    php7.2-odbc \
    php7.2-opcache \
    php7.2-pgsql \
    php7.2-phpdbg \
    php7.2-pspell \
    php7.2-readline \
    php7.2-recode \
    php7.2-soap \
    php7.2-sqlite3 \
    php7.2-tidy \
    php7.2-xml \
    php7.2-xmlrpc \
    php7.2-xsl \
    php7.2-zip && \
    apt-get clean && apt-get autoremove -y

# Install supervisord
RUN apt-get install -y supervisor

# Install Composer globally
RUN curl -sL https://getcomposer.org/installer | php \
    && mv composer.phar /usr/local/bin/composer \
    && chmod a+x /usr/local/bin/composer

# Let docker monitor the logs
RUN ln -snf /dev/stdout /var/log/fpm-php.www.log && \
    ln -snf /dev/stdout /var/log/php_error.log && \
    ln -snf /dev/stdout /var/log/php_cli_errors.log

# Clean up apt cache and temp files to save disk space
RUN apt-get clean && apt-get autoremove -y

# App specific part
COPY ./conf/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY ./conf/php-fpm.conf /etc/php/7.2/fpm/php-fpm.conf
COPY ./conf/www.conf /etc/php/7.2/fpm/pool.d/www.conf
COPY ./conf/php.ini /etc/php/7.2/cli/php.ini
COPY ./init_container.sh /root/init_container.sh

EXPOSE 9000

WORKDIR /var/www/html/projeszk

CMD /root/init_container.sh && \
    supervisord -c /etc/supervisor/conf.d/supervisord.conf && \
    php-fpm7.2 -c /etc/php/7.2/fpm/php-fpm.conf