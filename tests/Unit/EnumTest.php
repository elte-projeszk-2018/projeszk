<?php

namespace Tests\Unit;

use App\Enums\BaseEnum;
use App\Enums\Enum;
use App\Exceptions\ConstantNotFoundException;
use Tests\TestCase;

class EnumTest extends TestCase
{
    public function testGetEnum()
    {
        $foo = Test1Enum::FOO();
        $this->assertNotNull($foo);
        $this->assertEquals('FOO', $foo->getConstName());
        $this->assertEquals('foo', $foo->getConstValue());
    }

    public function testGetEnumByName()
    {
        $foo1 = Test1Enum::byName('FOO');

        $this->assertNotNull($foo1);
        $this->assertEquals('FOO', $foo1->getConstName());
        $this->assertEquals('foo', $foo1->getConstValue());

    }

    public function testEnumsAreTheSameObjects()
    {
        $foo1 = Test1Enum::FOO();
        $foo2 = Test1Enum::FOO();
        $this->assertSame($foo1, $foo2);
    }

    public function testEnumsAreTheSameObjects_callToByName()
    {
        $foo1 = Test1Enum::byName('FOO');
        $foo2 = Test1Enum::byName('FOO');
        $this->assertSame($foo1, $foo2);
    }

    public function testEnumsAreDifferentObjects()
    {
        $foo1 = Test1Enum::byName('FOO');
        $foo2 = Test2Enum::byName('FOO');
        $this->assertNotSame($foo1, $foo2);
    }

    public function testByNameThrowsOnNonExistingEnum()
    {
        $this->expectException(ConstantNotFoundException::class);
        Test1Enum::byName('BAR');
    }

    public function testEnumIsInstanceOf()
    {
        $foo1 = Test1Enum::byName('FOO');
        $this->assertTrue($foo1 instanceof Test1Enum, 'Retrieved enum is not instance of child class!');
        $this->assertTrue($foo1 instanceof BaseEnum, 'Retrieved enum is not instance of parent class!');
        $this->assertTrue($foo1 instanceof Enum, 'Retrieved enum is not implementing the Enum interface!');
    }
}

class Test1Enum extends BaseEnum
{
    const FOO = 'foo';
}

class Test2Enum extends BaseEnum
{
    const FOO = 'foo';
}