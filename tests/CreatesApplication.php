<?php

namespace Tests;

use App\Enums\EnvironmentEnum;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Console\Kernel;

trait CreatesApplication
{
    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     * @throws \App\Exceptions\ConstantNotFoundException
     * @throws \Exception
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();
        if (EnvironmentEnum::byName(strtoupper(\App::environment())) !== EnvironmentEnum::byName('TESTING')){
            throw new \Exception("Phpunit tried to run tests against non testing environment!");
        }
        Hash::driver('bcrypt')->setRounds(4);

        return $app;
    }
}
