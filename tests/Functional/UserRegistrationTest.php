<?php

namespace Tests\Functional;

use App\User;
use Illuminate\Foundation\Testing\Concerns\InteractsWithDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class UserRegistrationTest extends TestCase
{
    use DatabaseTransactions;
    use InteractsWithDatabase;

    const USER_REGISTRATION_ROUTE = '/api/users';

    public function testRegisterUser()
    {
        //Arrange
        $user = [
            'first_name' => 'FirstName',
            'last_name' => 'LastName',
            'email' => 'test@projeszk.com',
            'password' => 'secret',
        ];

        //Act
        $response = $this->post(self::USER_REGISTRATION_ROUTE, $user);

        //Assert
        $response->assertStatus(200);
        $response->assertJsonFragment(['first_name' => 'FirstName']);
        $response->assertJsonFragment(['last_name' => 'LastName']);
        $response->assertJsonFragment(['email' => 'test@projeszk.com']);
        $response->assertDontSee('password');
        $response->assertDontSee('remember_token');

        $this->assertDatabaseHas('users', ['email' => 'test@projeszk.com']);
    }

    public function testRegisterUser_invalidFirstName_shouldFail()
    {
        //Arrange
        $user = [
            'first_name' => 'F',
            'last_name' => 'LastName',
            'email' => 'test@projeszk.com',
            'password' => 'secret',
        ];

        //Act
        $response = $this->post(self::USER_REGISTRATION_ROUTE, $user);

        //Assert
        $response->assertStatus(412);
    }

    public function testRegisterUser_invalidLastName_shouldFail()
    {
        //Arrange
        $user = [
            'first_name' => 'FirstName',
            'last_name' => 'L',
            'email' => 'test@projeszk.com',
            'password' => 'secret',
        ];

        //Act
        $response = $this->post(self::USER_REGISTRATION_ROUTE, $user);

        //Assert
        $response->assertStatus(412);
    }

    public function testRegisterUser_invalidEmail_shouldFail()
    {
        //Arrange
        $user = [
            'first_name' => 'FirstName',
            'last_name' => 'LastName',
            'email' => 'test@projeszk',
            'password' => 'secret',
        ];

        //Act
        $response = $this->post(self::USER_REGISTRATION_ROUTE, $user);

        //Assert
        $response->assertStatus(412);
    }

    public function testRegisterUser_invalidPassword_shouldFail()
    {
        //Arrange
        $user = [
            'first_name' => 'FirstName',
            'last_name' => 'LastName',
            'email' => 'test@projeszk.com',
            'password' => '1234',
        ];

        //Act
        $response = $this->post(self::USER_REGISTRATION_ROUTE, $user);

        //Assert
        $response->assertStatus(412);
    }

    public function testRegisterUser_duplicatedEmail_shouldFail()
    {
        //Arrange

        factory(User::class)->create([
            'first_name' => 'FirstName',
            'last_name' => 'LastName',
            'email' => 'test@projeszk.com',
            'password' => 'password',
        ]);

        $user = [
            'first_name' => 'Test',
            'last_name' => 'Ede',
            'email' => 'test@projeszk.com',
            'password' => 'password',
        ];

        //Act
        $response = $this->post(self::USER_REGISTRATION_ROUTE, $user);

        //Assert
        $this->assertDatabaseHas('users', ['email' => 'test@projeszk.com']);
        $response->assertStatus(412);
    }
}
