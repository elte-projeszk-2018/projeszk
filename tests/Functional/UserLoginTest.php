<?php

namespace Tests\Functional;

use App\User;
use Illuminate\Foundation\Testing\Concerns\InteractsWithDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class UserLoginTest extends TestCase
{
    use DatabaseTransactions;
    use InteractsWithDatabase;

    public function testLogin()
    {
        //Arrange
        factory(User::class)->create([
            'first_name' => 'FirstName',
            'last_name' => 'LastName',
            'email' => 'test@projeszk.com',
            'password' => 'password',
            'remember_token' => str_random(10),
        ]);

        //Act
        $response = $this->post('/api/auth/login', [
            'email' => 'test@projeszk.com',
            'password' => 'password',
        ]);

        //Assert
        $this->assertDatabaseHas('users', ['email' => 'test@projeszk.com']);

        $response->assertStatus(200);
        $response->assertJsonFragment(["token_type" => "bearer"]);
        $response->assertSee("access_token");

        $response->assertSee('id');
        $response->assertSee('user');
        $response->assertSee('first_name');
        $response->assertSee('last_name');
        $response->assertSee('full_name');
        $response->assertSee('email');
        $response->assertDontSee('password');

        $data = json_decode($response->getContent(), true)['data'];
        $this->assertTrue(array_key_exists('user', $data));
        $this->assertTrue(is_array($data ['user']));
        $this->assertTrue(array_key_exists('first_name', $data['user']));
        $this->assertTrue(array_key_exists('last_name', $data['user']));
        $this->assertTrue(array_key_exists('full_name', $data['user']));
        $this->assertTrue(array_key_exists('email', $data['user']));
    }

    public function testLogin_missingUser_shouldFail()
    {
        //Arrange
        factory(User::class)->create([
            'first_name' => 'FirstName',
            'last_name' => 'LastName',
            'email' => 'test@projeszk.com',
            'password' => 'password',
            'remember_token' => str_random(10),
        ]);

        //Act
        $response = $this->post('/api/auth/login', [
            'email' => 'nonexistent@projeszk.com',
            'password' => 'password',
        ]);

        //Assert
        $this->assertDatabaseHas('users', ['email' => 'test@projeszk.com']);

        $response->assertStatus(401);
    }

    public function testLogin_wrongPassword_shouldFail()
    {
        //Arrange
        factory(User::class)->create([
            'first_name' => 'FirstName',
            'last_name' => 'LastName',
            'email' => 'test@projeszk.com',
            'password' => 'password',
            'remember_token' => str_random(10),
        ]);

        //Act
        $response = $this->post('/api/auth/login', [
            'email' => 'test@projeszk.com',
            'password' => '123456',
        ]);

        //Assert
        $this->assertDatabaseHas('users', ['email' => 'test@projeszk.com']);

        $response->assertStatus(401);
    }

    public function testLogin_notValidEmailAddress_shouldFail()
    {
        //Arrange

        //Act
        $response = $this->post('/api/auth/login', [
            'email' => 'test@projeszk',
            'password' => 'password',
        ]);

        //Assert
        $response->assertStatus(412);
    }

    public function testLogin_shortPassword_shouldFail()
    {
        //Arrange

        //Act
        $response = $this->post('/api/auth/login', [
            'email' => 'test@projeszk.com',
            'password' => '123',
        ]);

        //Assert
        $response->assertStatus(412);
    }
}
