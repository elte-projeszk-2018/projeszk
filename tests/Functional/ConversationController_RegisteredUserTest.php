<?php

namespace Tests\Functional;

use App\Models\Conversation;
use App\User;
use Illuminate\Foundation\Testing\Concerns\InteractsWithDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestWithLoggedInUser;

class ConversationController_RegisteredUserTest extends TestWithLoggedInUser
{
    use DatabaseTransactions;
    use InteractsWithDatabase;

    private const BASE_ROUTE = '/api/conversations';

    public function testIndex()
    {
        //Arrange
        $user = $this->getUser();
        $partner = factory(User::class)->create();
        $createConversationDto = [
            'user_ids' => [$partner->id],
        ];
        $response = $this->post(self::BASE_ROUTE, $createConversationDto, $this->getAuthorizationHeader());
        $conversation = json_decode($response->getContent(), true)['data'];

        //Act
        $response = $this->get(self::BASE_ROUTE, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(200);
        $response->assertSee('users');
        $response->assertSee('messages');
        $response->assertJsonFragment(['id' => $conversation['id']]);
        $response->assertJsonFragment(['admin_id' => $user->id]);
    }


    public function testStore_oneToOneChat()
    {
        $user = $this->getUser();
        //Arrange
        $partner = factory(User::class)->create();
        $createConversationDto = [
            'user_ids' => [$partner->id],
        ];

        //Act
        $response = $this->post(self::BASE_ROUTE, $createConversationDto, $this->getAuthorizationHeader());
        $conversation = json_decode($response->getContent(), true)['data'];

        //Assert
        $response->assertStatus(200);
        $response->assertSee('id');
        $response->assertSee('created_at');
        $response->assertSee('updated_at');
        $response->assertSee('admin_id');

        $this->assertDatabaseHas('conversations', ['admin_id' => $user->id]);
        $this->assertDatabaseHas('conversation_user', ['user_id' => $user->id, 'conversation_id' => $conversation['id']]);
        $this->assertDatabaseHas('conversation_user', ['user_id' => $partner->id, 'conversation_id' => $conversation['id']]);
        $count = $user->conversations()->get()->count();
        $this->assertTrue($count === 1, "$count conversations found!");
    }

    public function testStore_oneToOneChat_noDuplicates()
    {
        //Arrange
        $user = $this->getUser();
        $partner = factory(User::class)->create();
        $createConversationDto = [
            'user_ids' => [$partner->id],
        ];
        $response = $this->post(self::BASE_ROUTE, $createConversationDto, $this->getAuthorizationHeader());
        $response->assertStatus(200);

        //Act
        $response = $this->post(self::BASE_ROUTE, $createConversationDto, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(200);
        $count = $user->conversations()->get()->count();
        $this->assertTrue($count === 1, "$count conversations found!");
    }

    public function testStore_multiUserChat()
    {
        //Arrange
        $user = $this->getUser();
        $partner1 = factory(User::class)->create();
        $partner2 = factory(User::class)->create();
        $createConversationDto = [
            'user_ids' => [$partner1->id, $partner2->id],
        ];

        //Act
        $response = $this->post(self::BASE_ROUTE, $createConversationDto, $this->getAuthorizationHeader());
        $conversation = json_decode($response->getContent(), true)['data'];

        //Assert
        $response->assertStatus(200);
        $response->assertSee('id');
        $response->assertSee('created_at');
        $response->assertSee('updated_at');
        $response->assertSee('admin_id');

        $this->assertDatabaseHas('conversations', ['admin_id' => $user->id]);
        $this->assertDatabaseHas('conversation_user', ['user_id' => $user->id, 'conversation_id' => $conversation['id']]);
        $this->assertDatabaseHas('conversation_user', ['user_id' => $partner1->id, 'conversation_id' => $conversation['id']]);
        $this->assertDatabaseHas('conversation_user', ['user_id' => $partner2->id, 'conversation_id' => $conversation['id']]);
    }

    public function testStore_multiUserChat_noDuplicates()
    {
        //Arrange
        $user = $this->getUser();
        $partner1 = factory(User::class)->create();
        $partner2 = factory(User::class)->create();
        $createConversationDto = [
            'user_ids' => [$partner1->id, $partner2->id],
        ];
        $response = $this->post(self::BASE_ROUTE, $createConversationDto, $this->getAuthorizationHeader());
        $response->assertStatus(200);

        //Act
        $response = $this->post(self::BASE_ROUTE, $createConversationDto, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(200);
        $count = $user->conversations()->get()->count();
        $this->assertTrue($count === 1, "$count conversations found!");
    }

    public function testStore_noUsersGiven_shouldFail()
    {
        //Arrange
        $createConversationDto = [
            'user_ids' => [],
        ];

        //Act
        $response = $this->post(self::BASE_ROUTE, $createConversationDto, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(412);
    }

    public function testStore_selfGivenAsUser_shouldFail()
    {
        //Arrange
        $user = $this->getUser();
        $createConversationDto = [
            'user_ids' => [$user->id],
        ];

        //Act
        $response = $this->post(self::BASE_ROUTE, $createConversationDto, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(412);
    }

    public function testStore_oneToOneChat_notExistingUserGiven_shouldFail()
    {
        //Arrange
        $createConversationDto = [
            'user_ids' => [PHP_INT_MAX],
        ];

        //Act
        $response = $this->post(self::BASE_ROUTE, $createConversationDto, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(404);
    }

    public function testStore_multiUserChat_notExistingUserGiven_shouldFail()
    {
        //Arrange
        $partner1 = factory(User::class)->create();
        $createConversationDto = [
            'user_ids' => [$partner1->id, PHP_INT_MAX],
        ];

        //Act
        $response = $this->post(self::BASE_ROUTE, $createConversationDto, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(404);
    }

    public function testDestroy()
    {
        //Arrange
        $user = $this->getUser();
        $partner = factory(User::class)->create();
        $createConversationDto = [
            'user_ids' => [$partner->id],
        ];

        $response = $this->post(self::BASE_ROUTE, $createConversationDto, $this->getAuthorizationHeader());
        $response->assertStatus(200);
        $count = $user->conversations()->get()->count();
        $this->assertTrue($count === 1, "$count conversations found!");
        $conversation = json_decode($response->getContent(), true)['data'];

        //Act
        $response = $this->delete(self::BASE_ROUTE . "/{$conversation['id']}", [], $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(200);
        $count = $user->conversations()->get()->count();
        $this->assertTrue($count === 0, "$count conversations found!");
        $count = $partner->conversations()->get()->count();
        $this->assertTrue($count === 0, "Conversation pivot was not deleted!");
    }

    public function testDestroy_conversationNotFound_shouldFail()
    {
        //Arrange

        //Act
        $response = $this->delete(self::BASE_ROUTE . "/" . PHP_INT_MAX, [], $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(404);
    }

    public function testDestroy_canDeleteOwnedOnly_shouldFail()
    {
        //Arrange
        $user = $this->getUser();
        $partner = factory(User::class)->create();
        $createConversationDto = [
            'user_ids' => [$partner->id],
        ];

        $response = $this->post(self::BASE_ROUTE, $createConversationDto, $this->getAuthorizationHeader());
        $response->assertStatus(200);
        $count = $user->conversations()->get()->count();
        $this->assertTrue($count === 1, "$count conversations found!");
        $conversation = json_decode($response->getContent(), true)['data'];

        //Act
        $response = $this->delete(self::BASE_ROUTE . "/{$conversation['id']}", [],
            ['HTTP_AUTHORIZATION' => 'Bearer ' . \JWTAuth::fromUser($partner)]
            );

        //Assert
        $response->assertStatus(403);
        $count = $user->conversations()->get()->count();
        $this->assertTrue($count === 1, "$count conversations found!");
    }

    public function testAddUserToConversation()
    {
        //Arrange
        $user = $this->getUser();
        $partner1 = factory(User::class)->create();
        $createConversationDto = [
            'user_ids' => [$partner1->id],
        ];
        $response = $this->post(self::BASE_ROUTE, $createConversationDto, $this->getAuthorizationHeader());
        $conversation = json_decode($response->getContent(), true)['data'];
        $response->assertStatus(200);
        $this->assertDatabaseHas('conversations', ['admin_id' => $user->id]);
        $this->assertDatabaseHas('conversation_user', ['user_id' => $user->id, 'conversation_id' => $conversation['id']]);
        $this->assertDatabaseHas('conversation_user', ['user_id' => $partner1->id, 'conversation_id' => $conversation['id']]);

        $partner2 = factory(User::class)->create();
        $this->assertDatabaseMissing('conversation_user', ['user_id' => $partner2->id, 'conversation_id' => $conversation['id']]);
        $request = [
            'user_id' => $partner2->id
        ];
        //Act
        $response = $this->post(self::BASE_ROUTE . "/{$conversation['id']}/user", $request, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(200);
        $this->assertDatabaseHas('conversation_user', ['user_id' => $partner2->id, 'conversation_id' => $conversation['id']]);
    }

    public function testAddUserToConversation_noDuplicateUser_shouldFail()
    {
        //Arrange
        $user = $this->getUser();
        $partner1 = factory(User::class)->create();
        $createConversationDto = [
            'user_ids' => [$partner1->id],
        ];
        $response = $this->post(self::BASE_ROUTE, $createConversationDto, $this->getAuthorizationHeader());
        $conversation = json_decode($response->getContent(), true)['data'];
        $response->assertStatus(200);
        $this->assertDatabaseHas('conversations', ['admin_id' => $user->id]);
        $this->assertDatabaseHas('conversation_user', ['user_id' => $user->id, 'conversation_id' => $conversation['id']]);
        $this->assertDatabaseHas('conversation_user', ['user_id' => $partner1->id, 'conversation_id' => $conversation['id']]);
        $request = [
            'user_id' => $partner1->id
        ];
        //Act
        $response = $this->post(self::BASE_ROUTE . "/{$conversation['id']}/user", $request, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(403);
    }

    public function testAddUserToConversation_noUserGiven_shouldFail()
    {
        //Arrange
        $user = $this->getUser();
        $partner1 = factory(User::class)->create();
        $createConversationDto = [
            'user_ids' => [$partner1->id],
        ];
        $response = $this->post(self::BASE_ROUTE, $createConversationDto, $this->getAuthorizationHeader());
        $conversation = json_decode($response->getContent(), true)['data'];
        $response->assertStatus(200);
        $this->assertDatabaseHas('conversations', ['admin_id' => $user->id]);
        $this->assertDatabaseHas('conversation_user', ['user_id' => $user->id, 'conversation_id' => $conversation['id']]);
        $this->assertDatabaseHas('conversation_user', ['user_id' => $partner1->id, 'conversation_id' => $conversation['id']]);
        $request = [
        ];
        //Act
        $response = $this->post(self::BASE_ROUTE . "/{$conversation['id']}/user", $request, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(412);
    }

    public function testAddUserToConversation_noSuchUser_shouldFail()
    {
        //Arrange
        $user = $this->getUser();
        $partner1 = factory(User::class)->create();
        $createConversationDto = [
            'user_ids' => [$partner1->id],
        ];
        $response = $this->post(self::BASE_ROUTE, $createConversationDto, $this->getAuthorizationHeader());
        $conversation = json_decode($response->getContent(), true)['data'];
        $response->assertStatus(200);
        $this->assertDatabaseHas('conversations', ['admin_id' => $user->id]);
        $this->assertDatabaseHas('conversation_user', ['user_id' => $user->id, 'conversation_id' => $conversation['id']]);
        $this->assertDatabaseHas('conversation_user', ['user_id' => $partner1->id, 'conversation_id' => $conversation['id']]);
        $request = [
            'user_id' => PHP_INT_MAX,
        ];
        //Act
        $response = $this->post(self::BASE_ROUTE . "/{$conversation['id']}/user", $request, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(412);
    }

    public function testAddUserToConversation_noSuchConversation_shouldFail()
    {
        //Arrange
        $user = $this->getUser();
        $partner1 = factory(User::class)->create();
        $createConversationDto = [
            'user_ids' => [$partner1->id],
        ];
        $response = $this->post(self::BASE_ROUTE, $createConversationDto, $this->getAuthorizationHeader());
        $conversation = json_decode($response->getContent(), true)['data'];
        $response->assertStatus(200);
        $this->assertDatabaseHas('conversations', ['admin_id' => $user->id]);
        $this->assertDatabaseHas('conversation_user', ['user_id' => $user->id, 'conversation_id' => $conversation['id']]);
        $this->assertDatabaseHas('conversation_user', ['user_id' => $partner1->id, 'conversation_id' => $conversation['id']]);

        $partner2 = factory(User::class)->create();
        $this->assertDatabaseMissing('conversation_user', ['user_id' => $partner2->id, 'conversation_id' => $conversation['id']]);
        $request = [
            'user_id' => $partner2->id
        ];
        //Act
        $response = $this->post(self::BASE_ROUTE . "/" . PHP_INT_MAX . "/user", $request, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(404);
    }

    public function testAddUserToConversation_noDuplicatedConversations_shouldFail()
    {
        //Arrange
        /* conversation with partner1 */
        $partner1 = factory(User::class)->create();
        $createConversationDto = [
            'user_ids' => [$partner1->id],
        ];
        $response = $this->post(self::BASE_ROUTE, $createConversationDto, $this->getAuthorizationHeader());
        $conversation1 = json_decode($response->getContent(), true)['data'];
        $response->assertStatus(200);

        /* conversation with partner1 and partner2 */
        $partner2 = factory(User::class)->create();
        $createConversationDto = [
            'user_ids' => [$partner1->id, $partner2->id],
        ];
        $response = $this->post(self::BASE_ROUTE, $createConversationDto, $this->getAuthorizationHeader());
        $conversation2 = json_decode($response->getContent(), true)['data'];
        $response->assertStatus(200);
        $this->assertTrue($conversation1['id'] !== $conversation2['id']);

        $request = [
            'user_id' => $partner2->id
        ];
        //Act
        $response = $this->post(self::BASE_ROUTE . "/{$conversation1['id']}/user", $request, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(409);
    }

    public function testShowConversation()
    {
        //Arrange
        $partner1 = factory(User::class)->create();
        $partner2 = factory(User::class)->create();
        $createConversationDto = [
            'user_ids' => [$partner1->id, $partner2->id],
        ];
        $response = $this->post(self::BASE_ROUTE, $createConversationDto, $this->getAuthorizationHeader());
        $conversation = json_decode($response->getContent(), true)['data'];
        $response->assertStatus(200);

        //Act
        $response = $this->get(self::BASE_ROUTE . "/{$conversation['id']}", $this->getAuthorizationHeader());
        $conversation = json_decode($response->getContent(), true)['data'];

        //Assert
        $response->assertStatus(200);
        $response->assertSee('id');
        $response->assertSee('created_at');
        $response->assertSee('updated_at');
        $response->assertSee('admin_id');
        $response->assertSee('messages');
        $response->assertSee('users');

        $this->assertTrue(is_array($conversation['users']) && count($conversation['users']) === 3);
        $this->assertTrue(is_array($conversation['messages']) && count($conversation['messages']) === 0);
    }

    public function testShowConversation_noSuchConversation_shouldFail()
    {
        //Arrange

        //Act
        $response = $this->get(self::BASE_ROUTE . "/" . PHP_INT_MAX, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(404);
    }

    public function testAddMessage_oneToOneChat()
    {
        $user = $this->getUser();
        //Arrange
        $partner = factory(User::class)->create();
        $createConversationDto = [
            'user_ids' => [$partner->id],
        ];

        $response = $this->post(self::BASE_ROUTE, $createConversationDto, $this->getAuthorizationHeader());
        $response->assertStatus(200);
        $conversation = json_decode($response->getContent(), true)['data'];

        //Act
        $request = [
            'message' => 'test message for y\'all'
        ];
        $response = $this->post(self::BASE_ROUTE . "/{$conversation['id']}/messages", $request, $this->getAuthorizationHeader());
        $response->assertStatus(200);

        //Assert
        $response->assertSee('id');
        $response->assertSee('created_at');
        $response->assertSee('updated_at');
        $response->assertSee('owner_id');
        $response->assertJsonFragment(['message' => 'test message for y\'all']);

        $this->assertDatabaseHas('messages', ['owner_id' => $user->id, 'conversation_id' => $conversation['id']]);
        $count = $user->conversations()->find($conversation['id'])->messages->count();
        $this->assertTrue($count === 1, "$count message(s) found!");
    }
}
