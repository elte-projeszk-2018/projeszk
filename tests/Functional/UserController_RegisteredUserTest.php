<?php

namespace Tests\Functional;

use Illuminate\Foundation\Testing\Concerns\InteractsWithDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use JWTAuth;
use Tests\TestWithLoggedInUser;

class UserController_RegisteredUserTest extends TestWithLoggedInUser
{
    use DatabaseTransactions;
    use InteractsWithDatabase;

    private const USER_DATA_ROUTE = '/api/auth/me';
    private const LIST_USERS_ROUTE = '/api/users';
    private const UPDATE_USER_ROUTE = '/api/users';
    private const DELETE_SELF_ROUTE = '/api/users';
    private const CHANGE_PASSWORD_ROUTE = 'api/users/change-password';

    public function testIndex()
    {
        //Arrange
        $user = $this->getUser();

        //Act
        $response = $this->get(self::LIST_USERS_ROUTE, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(200);
        $response->assertJsonFragment(['first_name' => $user->first_name]);
        $response->assertJsonFragment(['last_name' => $user->last_name]);
        $response->assertJsonFragment(['full_name' => $user->full_name]);
        $response->assertJsonFragment(['email' => $user->email]);
        $response->assertSee('id');
        $response->assertDontSee('password');

        $users = json_decode($response->getContent(), true)['data'];
        $this->assertTrue(is_array($users) && count($users) >= 1);
    }

    public function testShow()
    {
        //Arrange
        $user = $this->getUser();

        //Act
        $response = $this->get(self::USER_DATA_ROUTE, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(200);
        $response->assertJsonFragment(['first_name' => $user->first_name]);
        $response->assertJsonFragment(['last_name' => $user->last_name]);
        $response->assertJsonFragment(['full_name' => $user->full_name]);
        $response->assertJsonFragment(['email' => $user->email]);
        $response->assertSee('id');
        $response->assertDontSee('password');
    }

    public function testUpdate()
    {
        //Arrange
        $user = $this->getUser();
        $updatedFirstName = 'updated.' . $user->first_name;
        $updatedLastName = 'updated.' . $user->last_name;
        $originalEmail = $user->email;

        $updatedUserData = [
            'first_name' => $updatedFirstName,
            'last_name' => $updatedLastName,
            'email' => 'updated.' . $originalEmail,
        ];

        //Act
        $response = $this->put(self::UPDATE_USER_ROUTE, $updatedUserData, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(200);
        $response->assertJsonFragment(['first_name' => 'updated.' . $user->first_name]);
        $response->assertJsonFragment(['last_name' => 'updated.' . $user->last_name]);
        $response->assertJsonFragment(['email' => $user->email]);
        $response->assertSee('id');
        $response->assertSee('full_name');
        $response->assertDontSee('password');

        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'email' => $originalEmail,
            'first_name' => $updatedFirstName,
            'last_name' => $updatedLastName,
        ]);
    }

    public function testUpdate_missingFirstName_shouldFail()
    {
        //Arrange
        $user = $this->getUser();
        $updatedUserData = [
            'last_name' => 'updated.' . $user->last_name,
            'email' => 'updated.' . $user->email,
        ];

        //Act
        $response = $this->put(self::UPDATE_USER_ROUTE, $updatedUserData, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(412);
    }

    public function testUpdate_invalidFirstName_shouldFail()
    {
        //Arrange
        $user = $this->getUser();
        $updatedUserData = [
            'first_name' => 'F',
            'last_name' => 'updated.' . $user->last_name,
            'email' => 'updated.' . $user->email,
        ];

        //Act
        $response = $this->put(self::UPDATE_USER_ROUTE, $updatedUserData, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(412);
    }

    public function testUpdate_invalidLastName_shouldFail()
    {
        //Arrange
        $user = $this->getUser();
        $updatedUserData = [
            'first_name' => 'updated.' . $user->first_name,
            'last_name' => '',
            'email' => 'updated.' . $user->email,
        ];

        //Act
        $response = $this->put(self::UPDATE_USER_ROUTE, $updatedUserData, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(412);
    }

    public function testUpdate_missingLastName_shouldFail()
    {
        //Arrange
        $user = $this->getUser();
        $updatedUserData = [
            'first_name' => 'updated.' . $user->first_name,
            'last_name' => 'L',
            'email' => 'updated.' . $user->email,
        ];

        //Act
        $response = $this->put(self::UPDATE_USER_ROUTE, $updatedUserData, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(412);
    }

    public function testChangePassword()
    {
        //Arrange
        $user = $this->getUser();
        $oldPwd = 'mypass';
        $newPwd = 'mynewpass';
        $passwordChangeDto = [
            'old_password' => $oldPwd,
            'new_password' => $newPwd,
        ];

        //Act
        $response = $this->put(self::CHANGE_PASSWORD_ROUTE, $passwordChangeDto, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(200);
        $this->assertFalse(!!JWTAuth::attempt([
            'email' => $user->email,
            'password' => $oldPwd,
        ]));
        $this->assertTrue(!!JWTAuth::attempt([
            'email' => $user->email,
            'password' => $newPwd,
        ]));
    }

    public function testChangePassword_invalidOldPassword_shouldFail()
    {
        //Arrange
        $passwordChangeDto = [
            'old_password' => 'asd',
            'new_password' => 'mynewpass',
        ];

        //Act
        $response = $this->put(self::CHANGE_PASSWORD_ROUTE, $passwordChangeDto, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(412);
    }

    public function testChangePassword_incorrectOldPassword_shouldFail()
    {
        //Arrange
        $passwordChangeDto = [
            'old_password' => 'mypass1',
            'new_password' => 'mynewpass',
        ];

        //Act
        $response = $this->put(self::CHANGE_PASSWORD_ROUTE, $passwordChangeDto, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(412);
    }

    public function testChangePassword_missingOldPassword_shouldFail()
    {
        //Arrange
        $passwordChangeDto = [
            'new_password' => 'mynewpass',
        ];

        //Act
        $response = $this->put(self::CHANGE_PASSWORD_ROUTE, $passwordChangeDto, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(412);
    }

    public function testChangePassword_invalidNewPassword_shouldFail()
    {
        //Arrange
        $passwordChangeDto = [
            'old_password' => 'mypass',
            'new_password' => 'asd',
        ];

        //Act
        $response = $this->put(self::CHANGE_PASSWORD_ROUTE, $passwordChangeDto, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(412);
    }

    public function testChangePassword_missingNewPassword_shouldFail()
    {
        //Arrange
        $passwordChangeDto = [
            'old_password' => 'mypass',
        ];

        //Act
        $response = $this->put(self::CHANGE_PASSWORD_ROUTE, $passwordChangeDto, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(412);
    }

    public function testDelete()
    {
        //Arrange
        $user = $this->getUser();
        $this->assertDatabaseHas('users', [
            'email' => $user->email
        ]);

        //Act
        $response = $this->delete(self::DELETE_SELF_ROUTE, [], $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(200);
        $this->assertDatabaseMissing('users', [
            'email' => $user->email
        ]);
    }
}
