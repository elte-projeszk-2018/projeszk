<?php

namespace Tests\Functional;

use Illuminate\Foundation\Testing\Concerns\InteractsWithDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class UserController_GuestUserTest extends TestCase
{
    use DatabaseTransactions;
    use InteractsWithDatabase;

    private const USER_DATA_ROUTE = '/api/auth/me';
    private const LIST_USERS_ROUTE = '/api/users';
    private const UPDATE_USER_ROUTE = '/api/users';
    private const DELETE_SELF_ROUTE = '/api/users';
    private const CHANGE_PASSWORD_ROUTE = 'api/users/change-password';

    public function testIndex_unauthorized_shouldFail()
    {
        //Arrange

        //Act
        $response = $this->get(self::LIST_USERS_ROUTE);

        //Assert
        $response->assertStatus(401);
    }

    public function testShow_unauthorized_shouldFail()
    {
        //Arrange

        //Act
        $response = $this->get(self::USER_DATA_ROUTE);

        //Assert
        $response->assertStatus(401);
    }

    public function testUpdate_unauthorized_shouldFail()
    {
        //Arrange

        //Act
        $response = $this->get(self::UPDATE_USER_ROUTE);

        //Assert
        $response->assertStatus(401);
    }

    public function testChangePassword_unauthorized_shouldFail()
    {
        //Arrange

        //Act
        $response = $this->put(self::CHANGE_PASSWORD_ROUTE);

        //Assert
        $response->assertStatus(401);
    }

    public function testDelete_unauthorized_shouldFail()
    {
        //Arrange

        //Act
        $response = $this->delete(self::DELETE_SELF_ROUTE);

        //Assert
        $response->assertStatus(401);
    }
}
