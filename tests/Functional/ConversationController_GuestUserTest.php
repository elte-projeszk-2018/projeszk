<?php

namespace Tests\Functional;

use Illuminate\Foundation\Testing\Concerns\InteractsWithDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class ConversationController_GuestUserTest extends TestCase
{
    use DatabaseTransactions;
    use InteractsWithDatabase;

    private const BASE_ROUTE = '/api/conversations';

    public function testIndex_unauthorized_shouldFail()
    {
        //Arrange

        //Act
        $response = $this->get(self::BASE_ROUTE);

        //Assert
        $response->assertStatus(401);
    }

    public function testStore_unauthorized_shouldFail()
    {
        //Arrange
        $createConversationDto = [
            'user_ids' => [1, 2, 3],
        ];

        //Act
        $response = $this->post(self::BASE_ROUTE, $createConversationDto);

        //Assert
        $response->assertStatus(401);
    }

    public function testDestroy_unauthorized_shouldFail()
    {
        //Arrange

        //Act
        $response = $this->delete(self::BASE_ROUTE . "/1");

        //Assert
        $response->assertStatus(401);
    }

    public function testAddUserToConversation_unauthorized_shouldFail()
    {
        //Arrange

        //Act
        $response = $this->post(self::BASE_ROUTE . "/1/user", []);

        //Assert
        $response->assertStatus(401);
    }

    public function testShowConversation_unauthorized_shouldFail()
    {
        //Arrange

        //Act
        $response = $this->get(self::BASE_ROUTE . "/1");

        //Assert
        $response->assertStatus(401);
    }
}
